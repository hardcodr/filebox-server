1. Hard delete deletes unrelated file [CRITICAL]
------------------------------------------------
if two file had same content then they are saved using single file in filestore (since there SHA matches). But if the one of the file is hard deleted then it does not check the other files reference and just deletes the file. Some form of reference counting should be done.
