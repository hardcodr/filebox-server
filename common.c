#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include <time.h>


#include "common.h"

const int BT_SIZE = 128;
const char *log_level[]= {"FATAL", "ERROR", "WARN", "INFO", "DEBUG"};

FILE *logfile = 0;

static void print_backtrace(FILE *logfile) {
    int i, nptrs;
    void *callstack[BT_SIZE];
    char **strings;

    nptrs = backtrace(callstack, BT_SIZE);
    strings = backtrace_symbols(callstack, nptrs);
    if(nptrs == 0 || strings == 0) {
        fprintf(logfile, "<<failed get stacktrace>>\n");
        return;
    }
    for (i = 0; i < nptrs; i++)
        fprintf(logfile, "%s\n", strings[i]);

    free(strings);
}

int fbx_log_verbosity = FBX_LOG_DEBUG;
void fbx_log(int level, const char *fmt, ...) {
    if(logfile == 0) {
        logfile = fopen("filebox.log", "a+");
        if(!logfile) {
            logfile = stderr;
        }
    }

    va_list ap;
    char msg[FBX_MAX_LOGMSG_LEN];
    time_t t;
    struct tm *tm;

    if ((level & 0xff) > fbx_log_verbosity) return;

    va_start(ap, fmt);
    vsnprintf(msg, sizeof(msg), fmt, ap);
    va_end(ap);

    t = time(0);
    tm = localtime(&t);

    fprintf(logfile, "[%d-%d-%d %d:%d:%d] %s: %s\n", tm->tm_year + 1900, tm->tm_mon + 1, \
        tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec, log_level[level], msg);

    print_backtrace(logfile);

    fflush(logfile);
}

static int split_str(const char *str, int len, char sep, char **key, char **value) {
    const char *loc = strchr(str, sep);
    if(loc) {
        int key_len = loc - str;
        int value_len = len - key_len - 1;

        if(key_len >= len) {
            return 0;
        }

        char *tkey = (char *)malloc(sizeof(char)*(key_len + 1));
        if(!tkey) {
            return 0;
        }
        char *tvalue = (char *)malloc(sizeof(char)*(value_len + 1));
        if(!tvalue) {
            free(tkey);
            return 0;
        }

        sprintf(tkey, "%.*s", key_len, str);
        sprintf(tvalue, "%.*s", value_len, loc + 1);

        *key = tkey;
        *value = tvalue;

        return 1;
    }

    return 0;
}

static int next_line_len(const char *buf, int len, int offset) {
    const char *loc = strchr(buf + offset, '\n');
    if(loc) {
        return loc - (buf + offset);
    } else {
        return len - offset;
    }
}

int read_conf(const char *conf_file, char **git_repo, 
        char **synclog_host, int *synclog_port, 
        int *location_count, char ***loc_ids, char ***locs, 
        int *init_sleeptime, int *max_sleeptime) {

    int i = 0;
    char *conf = 0;
    int conf_len = 0;
    if(FILEOP_FAILURE == file_read(conf_file, &conf, &conf_len)) {
        fprintf(stderr, "can't find conf file = %s\n", conf_file);
        return 1;
    }

    char *tmp_git_repo = 0;

    char *tmp_synclog_host = 0;
    int tmp_synclog_port = 0;

    int tmp_location_count = 0;
    char **tmp_loc_ids;
    char **tmp_locations;

    int offset = 0;
    int current_loc = 0;
    while(offset < conf_len) {
        int line_len = next_line_len(conf, conf_len, offset);
    
        if((conf + offset)[0] != '#' && line_len > 0) {
            char *key; char *value;
            if(split_str(conf + offset, line_len, '=', &key, &value)) {
                if(strcmp("gitrepo.path", key) == 0) {
                    tmp_git_repo = strdup(value);
                } else if(strcmp("synclog.host", key) == 0) {
                    tmp_synclog_host = strdup(value);
                } else if(strcmp("synclog.port", key) == 0) {
                    tmp_synclog_port = atoi(value);
                } else if(strcmp("location.count", key) == 0) {
                    tmp_location_count = atoi(value);
                    tmp_loc_ids = (char **)malloc(sizeof(char *)*tmp_location_count);
                    tmp_locations = (char **)malloc(sizeof(char *)*tmp_location_count);
                } else if(strncmp("location.", key, strlen("location.")) == 0) {
                    if(current_loc < tmp_location_count) {
                        tmp_loc_ids[current_loc] = strdup(key + strlen("location."));
                        tmp_locations[current_loc] = strdup(value);
                        current_loc++;
                    }
                } else if(strcmp("sync_daemon.init_sleep", key) == 0) {
                    //in nanoseconds
                    if(init_sleeptime) {
                        *init_sleeptime = atol(value);
                    }
                } else if(strcmp("sync_daemon.max_sleep", key) == 0) {
                    //in seconds
                    if(max_sleeptime) {
                        *max_sleeptime = atoi(value);
                    }
                } 

                free(key);
                free(value);
            } 
        }

        offset += line_len + 1; // + 1, for getting past '\n'
    }
    free(conf);

    int missing_prop = 0;
    if(!tmp_synclog_host) {
        fprintf(stderr, "synclog.host property missing\n");
        missing_prop = 1;
    }

    if(!tmp_synclog_port) {
        fprintf(stderr, "synclog.port property missing\n");
        missing_prop = 1;
    }

    if(!tmp_location_count) {
        fprintf(stderr, "location.count property missing\n");
        missing_prop = 1;
    }

    if(!tmp_git_repo) {
        fprintf(stderr, "gitrepo.path property missing\n");
        missing_prop = 1;
    }


    for(i = 0; i < tmp_location_count; i++) {
        if(!tmp_loc_ids[i] || !tmp_locations[i]) {
            fprintf(stderr, "location.<loc_id> property missing."
                " specified location count = %d\n", tmp_location_count);
            missing_prop = 1;
        }
    }

    if(!missing_prop) {
        if(git_repo) {
            *git_repo = tmp_git_repo;
        }

        if(synclog_host && synclog_port) {
            *synclog_host = tmp_synclog_host;
            *synclog_port = tmp_synclog_port;
        }

        if(location_count && loc_ids && locs) {
            *location_count = tmp_location_count;
            *loc_ids = tmp_loc_ids;
            *locs = tmp_locations;
        }
    }
    
    return missing_prop;
}


/* ================ sha1.c ================ */
/*
SHA-1 in C
By Steve Reid <steve@edmweb.com>
100% Public Domain

Test Vectors (from FIPS PUB 180-1)
"abc"
  A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D
"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
  84983E44 1C3BD26E BAAE4AA1 F95129E5 E54670F1
A million repetitions of "a"
  34AA973C D4C4DAA4 F61EEB2B DBAD2731 6534016F
*/

/* #define LITTLE_ENDIAN * This should be #define'd already, if true. */
/* #define SHA1HANDSOFF * Copies data before messing with it. */

#ifdef _WIN32
   #include <stdint.h>    
   #define u_int32_t uint32_t
#endif
typedef struct {
    u_int32_t state[5];
    u_int32_t count[2];
    unsigned char buffer[64];
} SHA1_CTX;

#define SHA1HANDSOFF

#include <stdio.h>
#include <string.h>
#include <sys/types.h>	/* for u_int*_t */
#if defined(__sun)
#if defined(__GNUC__)
#include <math.h>
#undef isnan
#define isnan(x) \
     __extension__({ __typeof (x) __x_a = (x); \
     __builtin_expect(__x_a != __x_a, 0); })

#undef isfinite
#define isfinite(x) \
     __extension__ ({ __typeof (x) __x_f = (x); \
     __builtin_expect(!isnan(__x_f - __x_f), 1); })

#undef isinf
#define isinf(x) \
     __extension__ ({ __typeof (x) __x_i = (x); \
     __builtin_expect(!isnan(__x_i) && !isfinite(__x_i), 0); })

#define u_int uint
#define u_int32_t uint32_t
#endif /* __GNUC__ */
#endif
#ifdef _WIN64
#include <sys/param.h>
#endif

#ifndef BYTE_ORDER
#if (BSD >= 199103)
# include <machine/endian.h>
#else
#if defined(linux) || defined(__linux__)
# include <endian.h>
#else
#define	LITTLE_ENDIAN	1234	/* least-significant byte first (vax, pc) */
#define	BIG_ENDIAN	4321	/* most-significant byte first (IBM, net) */
#define	PDP_ENDIAN	3412	/* LSB first in word, MSW first in long (pdp)*/

#if defined(vax) || defined(ns32000) || defined(sun386) || defined(__i386__) || \
    defined(MIPSEL) || defined(_MIPSEL) || defined(BIT_ZERO_ON_RIGHT) || \
    defined(__alpha__) || defined(__alpha)
#define BYTE_ORDER	LITTLE_ENDIAN
#endif

#if defined(sel) || defined(pyr) || defined(mc68000) || defined(sparc) || \
    defined(is68k) || defined(tahoe) || defined(ibm032) || defined(ibm370) || \
    defined(MIPSEB) || defined(_MIPSEB) || defined(_IBMR2) || defined(DGUX) ||\
    defined(apollo) || defined(__convex__) || defined(_CRAY) || \
    defined(__hppa) || defined(__hp9000) || \
    defined(__hp9000s300) || defined(__hp9000s700) || \
    defined (BIT_ZERO_ON_LEFT) || defined(m68k) || defined(__sparc)
#define BYTE_ORDER	BIG_ENDIAN
#endif
#endif /* linux */
#endif /* BSD */
#endif /* BYTE_ORDER */

#if defined(__BYTE_ORDER) && !defined(BYTE_ORDER)
#if (__BYTE_ORDER == __LITTLE_ENDIAN)
#define BYTE_ORDER LITTLE_ENDIAN
#else
#define BYTE_ORDER BIG_ENDIAN
#endif
#endif

#ifdef _MSC_VER
    #ifndef BYTE_ORDER
    #define BYTE_ORDER LITTLE_ENDIAN
    #endif
#endif

#if !defined(BYTE_ORDER) || \
    (BYTE_ORDER != BIG_ENDIAN && BYTE_ORDER != LITTLE_ENDIAN && \
    BYTE_ORDER != PDP_ENDIAN)
	/* you must determine what the correct bit order is for
	 * your compiler - the next line is an intentional error
	 * which will force your compiles to bomb until you fix
	 * the above macros.
	 */
#error "Undefined or invalid BYTE_ORDER"
#endif

#define rol(value, bits) (((value) << (bits)) | ((value) >> (32 - (bits))))

/* blk0() and blk() perform the initial expand. */
/* I got the idea of expanding during the round function from SSLeay */
#if BYTE_ORDER == LITTLE_ENDIAN
#define blk0(i) (block->l[i] = (rol(block->l[i],24)&0xFF00FF00) \
    |(rol(block->l[i],8)&0x00FF00FF))
#elif BYTE_ORDER == BIG_ENDIAN
#define blk0(i) block->l[i]
#else
#error "Endianness not defined!"
#endif
#define blk(i) (block->l[i&15] = rol(block->l[(i+13)&15]^block->l[(i+8)&15] \
    ^block->l[(i+2)&15]^block->l[i&15],1))

/* (R0+R1), R2, R3, R4 are the different operations used in SHA1 */
#define R0(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk0(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R1(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R2(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0x6ED9EBA1+rol(v,5);w=rol(w,30);
#define R3(v,w,x,y,z,i) z+=(((w|x)&y)|(w&x))+blk(i)+0x8F1BBCDC+rol(v,5);w=rol(w,30);
#define R4(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0xCA62C1D6+rol(v,5);w=rol(w,30);


/* Hash a single 512-bit block. This is the core of the algorithm. */

void SHA1Transform(u_int32_t state[5], const unsigned char buffer[64])
{
u_int32_t a, b, c, d, e;
typedef union {
    unsigned char c[64];
    u_int32_t l[16];
} CHAR64LONG16;
#ifdef SHA1HANDSOFF
CHAR64LONG16 block[1];  /* use array to appear as a pointer */
    memcpy(block, buffer, 64);
#else
    /* The following had better never be used because it causes the
     * pointer-to-const buffer to be cast into a pointer to non-const.
     * And the result is written through.  I threw a "const" in, hoping
     * this will cause a diagnostic.
     */
CHAR64LONG16* block = (const CHAR64LONG16*)buffer;
#endif
    /* Copy context->state[] to working vars */
    a = state[0];
    b = state[1];
    c = state[2];
    d = state[3];
    e = state[4];
    /* 4 rounds of 20 operations each. Loop unrolled. */
    R0(a,b,c,d,e, 0); R0(e,a,b,c,d, 1); R0(d,e,a,b,c, 2); R0(c,d,e,a,b, 3);
    R0(b,c,d,e,a, 4); R0(a,b,c,d,e, 5); R0(e,a,b,c,d, 6); R0(d,e,a,b,c, 7);
    R0(c,d,e,a,b, 8); R0(b,c,d,e,a, 9); R0(a,b,c,d,e,10); R0(e,a,b,c,d,11);
    R0(d,e,a,b,c,12); R0(c,d,e,a,b,13); R0(b,c,d,e,a,14); R0(a,b,c,d,e,15);
    R1(e,a,b,c,d,16); R1(d,e,a,b,c,17); R1(c,d,e,a,b,18); R1(b,c,d,e,a,19);
    R2(a,b,c,d,e,20); R2(e,a,b,c,d,21); R2(d,e,a,b,c,22); R2(c,d,e,a,b,23);
    R2(b,c,d,e,a,24); R2(a,b,c,d,e,25); R2(e,a,b,c,d,26); R2(d,e,a,b,c,27);
    R2(c,d,e,a,b,28); R2(b,c,d,e,a,29); R2(a,b,c,d,e,30); R2(e,a,b,c,d,31);
    R2(d,e,a,b,c,32); R2(c,d,e,a,b,33); R2(b,c,d,e,a,34); R2(a,b,c,d,e,35);
    R2(e,a,b,c,d,36); R2(d,e,a,b,c,37); R2(c,d,e,a,b,38); R2(b,c,d,e,a,39);
    R3(a,b,c,d,e,40); R3(e,a,b,c,d,41); R3(d,e,a,b,c,42); R3(c,d,e,a,b,43);
    R3(b,c,d,e,a,44); R3(a,b,c,d,e,45); R3(e,a,b,c,d,46); R3(d,e,a,b,c,47);
    R3(c,d,e,a,b,48); R3(b,c,d,e,a,49); R3(a,b,c,d,e,50); R3(e,a,b,c,d,51);
    R3(d,e,a,b,c,52); R3(c,d,e,a,b,53); R3(b,c,d,e,a,54); R3(a,b,c,d,e,55);
    R3(e,a,b,c,d,56); R3(d,e,a,b,c,57); R3(c,d,e,a,b,58); R3(b,c,d,e,a,59);
    R4(a,b,c,d,e,60); R4(e,a,b,c,d,61); R4(d,e,a,b,c,62); R4(c,d,e,a,b,63);
    R4(b,c,d,e,a,64); R4(a,b,c,d,e,65); R4(e,a,b,c,d,66); R4(d,e,a,b,c,67);
    R4(c,d,e,a,b,68); R4(b,c,d,e,a,69); R4(a,b,c,d,e,70); R4(e,a,b,c,d,71);
    R4(d,e,a,b,c,72); R4(c,d,e,a,b,73); R4(b,c,d,e,a,74); R4(a,b,c,d,e,75);
    R4(e,a,b,c,d,76); R4(d,e,a,b,c,77); R4(c,d,e,a,b,78); R4(b,c,d,e,a,79);
    /* Add the working vars back into context.state[] */
    state[0] += a;
    state[1] += b;
    state[2] += c;
    state[3] += d;
    state[4] += e;
    /* Wipe variables */
    a = b = c = d = e = 0;
#ifdef SHA1HANDSOFF
    memset(block, '\0', sizeof(block));
#endif
}


/* SHA1Init - Initialize new context */

void SHA1Init(SHA1_CTX* context)
{
    /* SHA1 initialization constants */
    context->state[0] = 0x67452301;
    context->state[1] = 0xEFCDAB89;
    context->state[2] = 0x98BADCFE;
    context->state[3] = 0x10325476;
    context->state[4] = 0xC3D2E1F0;
    context->count[0] = context->count[1] = 0;
}


/* Run your data through this. */

void SHA1Update(SHA1_CTX* context, const unsigned char* data, u_int32_t len)
{
u_int32_t i;
u_int32_t j;

    j = context->count[0];
    if ((context->count[0] += len << 3) < j)
	context->count[1]++;
    context->count[1] += (len>>29);
    j = (j >> 3) & 63;
    if ((j + len) > 63) {
        memcpy(&context->buffer[j], data, (i = 64-j));
        SHA1Transform(context->state, context->buffer);
        for ( ; i + 63 < len; i += 64) {
            SHA1Transform(context->state, &data[i]);
        }
        j = 0;
    }
    else i = 0;
    memcpy(&context->buffer[j], &data[i], len - i);
}


/* Add padding and return the message digest. */

void SHA1Final(unsigned char digest[20], SHA1_CTX* context)
{
unsigned i;
unsigned char finalcount[8];
unsigned char c;

#if 0	/* untested "improvement" by DHR */
    /* Convert context->count to a sequence of bytes
     * in finalcount.  Second element first, but
     * big-endian order within element.
     * But we do it all backwards.
     */
    unsigned char *fcp = &finalcount[8];

    for (i = 0; i < 2; i++)
    {
	u_int32_t t = context->count[i];
	int j;

	for (j = 0; j < 4; t >>= 8, j++)
	    *--fcp = (unsigned char) t
    }
#else
    for (i = 0; i < 8; i++) {
        finalcount[i] = (unsigned char)((context->count[(i >= 4 ? 0 : 1)]
         >> ((3-(i & 3)) * 8) ) & 255);  /* Endian independent */
    }
#endif
    c = 0200;
    SHA1Update(context, &c, 1);
    while ((context->count[0] & 504) != 448) {
	c = 0000;
        SHA1Update(context, &c, 1);
    }
    SHA1Update(context, finalcount, 8);  /* Should cause a SHA1Transform() */
    for (i = 0; i < 20; i++) {
        digest[i] = (unsigned char)
         ((context->state[i>>2] >> ((3-(i & 3)) * 8) ) & 255);
    }
    /* Wipe variables */
    memset(context, '\0', sizeof(*context));
    memset(&finalcount, '\0', sizeof(finalcount));
}
/* ================ end of sha1.c ================ */

#define SHABUFSIZE 4096

char *file_sha1(const char *filename) {
    int i;
    int fd;
    struct stat f_stat;
    ssize_t len;
    SHA1_CTX ctx;
    unsigned char hash[20], buf[SHABUFSIZE];

    fd = open(filename, O_RDONLY);
    if(fd == -1) {
        return NULL;
    }

    if(fstat(fd, &f_stat) == -1) {
        close(fd);
        return NULL;
    }

    SHA1Init(&ctx);
    
    for(len = f_stat.st_size;len > 0;) {
        ssize_t nread = read(fd, buf, (len < SHABUFSIZE ? len : SHABUFSIZE));
        if (nread == -1) {
            if (errno != EINTR) {
                close(fd);
                return NULL;
            } else {
                continue;
            }
        } 
        SHA1Update(&ctx, buf, nread);
        len -= nread;
    }
    close(fd);

    SHA1Final(hash, &ctx);

    char *filesha1 = (char *)malloc(sizeof(char)*41);
    for(i = 0; i < 20; i++)
        sprintf(filesha1 + 2*i, "%02x", hash[i]);
    filesha1[40] = '\0';

    return filesha1;
}

