#ifndef __FBX_COMMON_H__
#define __FBX_COMMON_H__

#include <stdio.h>
#include <stdarg.h>

#define FBX_MAX_LOGMSG_LEN 1024

#define FBX_LOG_DEBUG 4
#define FBX_LOG_INFO  3
#define FBX_LOG_WARN  2
#define FBX_LOG_ERROR 1
#define FBX_LOG_FATAL 0

extern int fbx_log_verbosity;
void fbx_log(int level, const char *fmt, ...);

/* computes the sha1 of the given file 
 * returns the sha1 if successful, NULL otherwise (err set in errno)
 */
char *file_sha1(const char *filename);

/* file IO relate common utils */
#define FILEOP_SUCCESS  0
#define FILEOP_FAILURE -1

int file_exists(const char *filename, int *exists);
int file_copy(const char *from, const char *to);
int file_move(const char *from, const char *to);
int file_delete(const char *filename);

int file_read(const char *filename, char **buf, int *bytes_read);
int file_write(const char *filename, char *buf, int bytes_to_write);

char *path_join(const char *parent, const char *child);
int path_mkdirs(char *path);

int read_conf(const char *str, char **git_repo, char **synclog_host, int *synclog_port, 
    int *location_count, char ***loc_ids, char ***locs, int *init_sleeptime, int *max_sleeptime);

#endif // __FBX_COMMON_H__
