#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <malloc.h>
#include <sys/sendfile.h>

#include <sds.h>

#include "common.h"

static const int READ_ONLY = O_RDONLY;
static const int CREATE_OR_TRUNCATE = O_WRONLY | O_CREAT | O_TRUNC;
#if defined(__unix__)
static const int DEFAULT_PERMISSION_FILE = S_IWUSR | S_IWGRP | S_IRUSR | S_IRGRP | S_IROTH;
static const int DEFAULT_PERMISSION_DIR = S_IRWXG | S_IRWXU | S_IROTH | S_IXOTH;
#else
static const int DEFAULT_PERMISSION_FILE = 0644;
static const int DEFAULT_PERMISSION_DIR = 0775;
#endif

/*
 Copies file content from source to destination location.
 Destination file should not be directory.
 If destination file exists then it will be truncated.
*/
int file_copy(const char *from, const char *to) {
    int ffd, tfd;
    off_t off = 0;
    struct stat f_stat;
    ssize_t len;

    ffd = open(from, READ_ONLY);
    if(ffd == -1) {
        return FILEOP_FAILURE;
    }

    tfd = open(to, CREATE_OR_TRUNCATE, DEFAULT_PERMISSION_FILE);
    if(tfd == -1) {
        close(ffd);
        return FILEOP_FAILURE;
    }

    if(fstat(ffd, &f_stat) == -1) {
        close(ffd); close(tfd);
        return FILEOP_FAILURE;
    }

    len = f_stat.st_size;
    for(;len > 0;) {
        ssize_t res;
        res = sendfile(tfd, ffd, &off, len);
        if (res == -1) {
            if (errno != EINTR) {
                close(ffd); close(tfd);
                return FILEOP_FAILURE;
            } else {
                continue;
            }
        }
        len -= res;
    }

#if defined(FILECOPY_HARD_SYNC) && defined(__unix__)
    if(fsync(tfd) == -1) {
        close(ffd); close(tfd);
        return FILEOP_FAILURE;
    }
#endif

   close(ffd);
   if(close(tfd) == -1) {
        return FILEOP_FAILURE;
   }

   return FILEOP_SUCCESS;
}

int file_delete(const char *filename) {
    if(remove(filename) == -1) {
        return FILEOP_FAILURE;
    }

    return FILEOP_SUCCESS;
}

int file_move(const char *from, const char *to) {
    //XXX: avoid file_copy when the 'from' and 'to' are on
    //same mounted device and just call rename(from,to)
    if(file_copy(from, to) == FILEOP_FAILURE) {
        return FILEOP_FAILURE;
    } 
    return file_delete(from);
}

int file_exists(const char *filename, int *exists) {
    struct stat f_stat;
    
    int reply = stat(filename, &f_stat);
    if(reply == 0) {
        *exists = 1;
    } else if(errno == ENOENT) {
        *exists = 0;
    } else {
        return FILEOP_FAILURE;
    }

    return FILEOP_SUCCESS;
}

/**
 * To join *nix style path (separator == '/')
 */
char *path_join(const char *parent, const char *child) {
    char *joinpath;
    int plen = strlen(parent);
    int clen = strlen(child);

    if(plen == 0 || parent[plen-1] == '/') {
        joinpath = (char *)malloc(plen + clen + 1);
        if(joinpath)
            sprintf(joinpath, "%s%s", parent, child);
    } else {
        joinpath = (char *)malloc(plen + 1 + clen + 1);
        if(joinpath)
            sprintf(joinpath, "%s/%s", parent, child);
    }
    return joinpath;
}

/**
 * creates directories in the given path (all the way to the top-most directory 
 * if entire hierarchy is not present). Assumes the path to be in *nix format.
 */
int path_mkdirs(char *path) {
    int retCode = FILEOP_SUCCESS;
    int i;

    int ndirs = 0;
    sds *dirs = sdssplitlen(path, strlen(path), "/", 1, &ndirs);
    if(ndirs && dirs) {
        int exists = 0;
        sds cpath;
        if(path[0] == '/') {
            cpath = sdsnew("/");
        }else {
            cpath = sdsempty();
        }

        for(i = 0; i < ndirs; i++) {
            cpath = sdscat(cpath, dirs[i]);
            cpath = sdscat(cpath, "/");
            if(FILEOP_SUCCESS == file_exists(cpath, &exists)) {
                if(!exists) {
#if defined(__unix__)
                    int res = mkdir(cpath, DEFAULT_PERMISSION_DIR);
#else
                    int res = mkdir(cpath);
#endif
                    if(0 != res && EEXIST != errno) {
                        retCode = FILEOP_FAILURE;
                        break;
                    }
                }
            } else {
                retCode = FILEOP_FAILURE;
                break; 
            }
        }
        sdsfree(cpath);
        for(i = 0; i < ndirs; i++) {
            sdsfree(dirs[i]);
        }
        free(dirs);
    }

    return retCode;
}

/**
 * reads the text file into the memory
 * output is stored in the *buf, and number of characters read is
 * in *bytes_read.
 */
int file_read(const char *filename, char **buf, int *bytes_read) {
    char buffer[4096];
    int in_fd = open(filename, READ_ONLY);
    if(in_fd == -1) {
        return FILEOP_FAILURE;
    }

    sds tmpbuf = sdsempty();
    for(;;) {
        int nread = read(in_fd, buffer, 4096);
        if(nread == -1) {
            if(errno == EINTR)
                continue;
            sdsfree(tmpbuf);
            close(in_fd);
            return FILEOP_FAILURE;
        } else if(nread == 0) {
            close(in_fd);
            break;
        } else {
            tmpbuf = sdscatlen(tmpbuf, buffer, nread);
        }
    }

    int nbytes = sdslen(tmpbuf);
    char *finalbuf = (char *)malloc(nbytes + 1);
    if(finalbuf == NULL) {
        sdsfree(tmpbuf);
        return FILEOP_FAILURE;
    }
    strcpy(finalbuf, tmpbuf);
    *buf = finalbuf;
    *bytes_read = nbytes;

    sdsfree(tmpbuf);
    return FILEOP_SUCCESS;
}

/**
 * writes string content in the given file
 */
int file_write(const char *filename, char *buf, int bytes_to_write) {
    int tmp = bytes_to_write;
    int out_fd = open(filename, CREATE_OR_TRUNCATE, DEFAULT_PERMISSION_FILE);
    if(out_fd == -1) {
        return FILEOP_FAILURE;
    }

    for(;tmp > 0;) {
        int nwrite = write(out_fd, buf + (bytes_to_write - tmp), tmp);
        if(nwrite == -1) {
            if(errno == EINTR)
                continue;
            close(out_fd);
            return FILEOP_FAILURE;
        }
        tmp -= nwrite;
    }

#if defined(FILEWRITE_HARD_SYNC) && defined(__unix__)
    if(fsync(out_fd) == -1) {
        close(out_fd);
        return FILEOP_FAILURE;
    }
#endif

    close(out_fd);
    return FILEOP_SUCCESS;
}

#if defined(TEST_FILE_COMMON) 
#include <stdio.h>
#include <stdlib.h>
#include "testhelp.h"

int main(void) {
    int exists;
    int reply;
    char *reply_str;
    struct stat f_stat;
    system("echo some sample text > file-common-test-file-1");

    reply = file_copy("file-common-test-file-1", "file-common-test-file-2");
    test_cond("file_copy", 
        reply == FILEOP_SUCCESS && stat("file-common-test-file-2", &f_stat) == 0);

    reply = file_exists("file-common-test-file-2", &exists);
    test_cond("file_exists", reply == FILEOP_SUCCESS && exists == 1);

    //system("diff file-common-test-file-1 file-common-test-file-2");

    reply = file_move("file-common-test-file-1", "file-common-test-file-3");
    test_cond("file_move", 
        reply == FILEOP_SUCCESS && stat("file-common-test-file-3", &f_stat) == 0);
    test_cond("file_move", 
        reply == FILEOP_SUCCESS && stat("file-common-test-file-1", &f_stat) != 0);

    system("diff file-common-test-file-3 file-common-test-file-2");

    reply = file_delete("file-common-test-file-3");
    test_cond("file_delete", 
        reply == FILEOP_SUCCESS && stat("file-common-test-file-3", &f_stat) != 0);

    reply = file_exists("file-common-test-file-3", &exists);
    test_cond("file_exists", reply == FILEOP_SUCCESS && exists == 0);

    reply = file_delete("file-common-test-file-2");
    test_cond("file_delete", 
        reply == FILEOP_SUCCESS && stat("file-common-test-file-2", &f_stat) != 0);

    reply = file_exists("file-common-test-file-2", &exists);
    test_cond("file_exists", reply == FILEOP_SUCCESS && exists == 0);

    reply_str = path_join("/test", "best/rest");
    test_cond("path_join", reply_str && 0 == strcmp(reply_str, "/test/best/rest"));
    if(reply_str)
        free(reply_str);

    reply_str = path_join("/test/", "best/rest");
    test_cond("path_join", reply_str && 0 == strcmp(reply_str, "/test/best/rest"));
    if(reply_str)
        free(reply_str);

    reply_str = path_join("", "best/rest");
    test_cond("path_join", reply_str && 0 == strcmp(reply_str, "best/rest"));
    if(reply_str)
        free(reply_str);

    reply = path_mkdirs("testdir/bestdir/restdir");
    test_cond("path_mkdirs", 
        reply == FILEOP_SUCCESS && stat("testdir/bestdir/restdir", &f_stat) == 0);

    system("rm -fR testdir");

    char *sometext = "this some text which needs to be written";
    int textsize = strlen(sometext);
    reply = file_write("file-rw-test", sometext, textsize);
    test_cond("file_write",
        reply == FILEOP_SUCCESS && stat("file-rw-test", &f_stat) == 0);

    char *readtext;
    int nreadchars;
    reply = file_read("file-rw-test", &readtext, &nreadchars);
    test_cond("file_read",
        reply == FILEOP_SUCCESS && strcmp(readtext, sometext) == 0 && nreadchars == textsize);
    if(readtext)
        free(readtext);

    system("rm -f file-rw-test");


    test_report();
    return 0;
}

#endif
