#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "common.h"
#include "synclog.h"
#include "filestore.h"

const float REPLICATION_FACTOR = 0.67;

static int is_present(char *heystack[], int nsize, char *needle) {
    int i;
    for(i = 0; i < nsize; i++) {
        if(0 == strcmp(heystack[i], needle))
            return 1;
    }
    return 0;
}

fstore_context *fstore_init(synclog_context *synclog, int nlocs, char *loc_ids[], char *locs[]) {
    fbx_log(FBX_LOG_DEBUG, "[fstore_init] enter");
    assert(nlocs > 0);
    
    //XXX: should fstore_context take ownsership
    //of loc_ids and locs?
    fstore_context *context = 
        (fstore_context *)malloc(sizeof(fstore_context));
    context->synclog = synclog;
    context->nlocs = nlocs;
    context->loc_ids = loc_ids;
    context->locs = locs;

    srand(time(NULL));

    fbx_log(FBX_LOG_DEBUG, "[fstore_init] exit");
    return context;
}

int fstore_store_request(fstore_context *context, char *file_id, char *sourcefile) {
    fbx_log(FBX_LOG_DEBUG, "[fstore_store_request] enter");

    int exists = 0;
    if(synclog_exists(context->synclog, file_id, &exists, NULL, NULL) == SYNCLOG_ERROR
        || (!exists && synclog_add(context->synclog, file_id, sourcefile) == SYNCLOG_ERROR)) {
        fbx_log(FBX_LOG_ERROR, "[fstore_store_request] synclog error, exit");
        return FILESTORE_OP_FAILURE;
    }

    fbx_log(FBX_LOG_DEBUG, "[fstore_store_request] exit");
    return FILESTORE_OP_SUCCESS;
}


int fstore_store(fstore_context *context, char *file_id, char *sourcefile) {
    fbx_log(FBX_LOG_DEBUG, "[fstore_store] enter");

    int retcode = FILESTORE_OP_FAILURE;

    int i;
    int nlocs = context->nlocs;
    char **loc_ids = context->loc_ids;
    char **locs = context->locs;

    char *targets[nlocs];
    int targets_base_loc[nlocs];
    char targets_base[512]; //XXX: bad! bad!! bad!!!
   
    for(i = 0; i < nlocs; i++) {
        targets[i] = (char *)malloc(strlen(locs[i]) + 1 + strlen(file_id) + 1 + 1);
        sprintf(targets[i], "%s/%.*s/%s", locs[i], 2, file_id, file_id + 2);
        targets_base_loc[i] = strlen(locs[i]) + 1 + 2;
    }

    int nfailed = 0;
    int failed[nlocs];
    for(i = 0; i < nlocs; i++) {
        failed[i] = 0;

        strncpy(targets_base, targets[i], targets_base_loc[i]);
        targets_base[targets_base_loc[i]] = '\0';

        int exists = 0;
        if(FILEOP_SUCCESS == file_exists(targets_base, &exists)) {
            if(!exists && FILEOP_SUCCESS == path_mkdirs(targets_base)) {
                exists = 1;
            }
        }

        if(exists && file_copy(sourcefile, targets[i]) == FILEOP_SUCCESS) {
            continue;
        }

        fbx_log(FBX_LOG_WARN, "[fstore_store] store failed from %s to %s",
            sourcefile, targets[i]);

        failed[i] = 1;
        nfailed++;
    }

    //we need replication by some factor to serve the file, hence return failure 
    //when file can't be copied to given number of locations.
    if(nfailed <= nlocs - (int)ceil(nlocs*REPLICATION_FACTOR)){
        for(i = 0; i < nlocs; i++) {
            if(!failed[i]) {
                //XXX:[reco] even if this synclog operation fails, reconcilition process
                //will take care of replication by checking file in stores.
                //XXX:[1] what if file is partially copied and synclog operation failed?
                synclog_sync_success(context->synclog, file_id, loc_ids[i]);
            }
        }
        retcode = FILESTORE_OP_SUCCESS;
    } else {
        fbx_log(FBX_LOG_ERROR, "[fstore_store] store failed from %d locations."
            " Can't continue", nfailed);
    }

    for(i = 0; i < nlocs; i++) {
        free(targets[i]);
    }

    fbx_log(FBX_LOG_DEBUG, "[fstore_store] exit");
    return retcode;
}

int fstore_retrieve(fstore_context *context, char *file_id, char *destfile)  {
    fbx_log(FBX_LOG_DEBUG, "[fstore_retrieve] enter");

    int i;
    int nlocs = context->nlocs;
    char **loc_ids = context->loc_ids;
    char **locs = context->locs;

    int exists;
    int nlocs_avail = 0;
    char **locs_avail = 0;

    if(synclog_exists(context->synclog, file_id, &exists, &locs_avail, &nlocs_avail) == SYNCLOG_ERROR
        || exists == SYNCLOG_NOT_EXISTS) {
        fbx_log(FBX_LOG_ERROR, "[fstore_retrieve] synclog error or not exist, exit");
        return FILESTORE_OP_FAILURE;
    }
    
    int max_try = 1 << ((nlocs < 10) ? nlocs : 10);
    for(i = 0; i < max_try; i++) {
        int randloc = rand() % nlocs;
        if(is_present(locs_avail, nlocs_avail, loc_ids[randloc])) {
            char *source = (char *)malloc(strlen(locs[randloc]) + 1 + strlen(file_id) + 1 + 1);
            sprintf(source, "%s/%.*s/%s", locs[randloc], 2, file_id, file_id + 2);

            int exists = 0;
            if(file_exists(source, &exists) == FILEOP_FAILURE 
                || !exists
                || file_copy(source, destfile) == FILEOP_FAILURE) {
                free(source);
                continue;
            }
            //XXX:[1] calculate file-sha and check with file_id 
            free(source);
            break;
        }
    }

    if(locs_avail) {
        for(i = 0; i < nlocs_avail; i++) {
            free(locs_avail[i]);
        }
        free(locs_avail);
    } else {
        fbx_log(FBX_LOG_ERROR, "[fstore_retrieve] file not available at any location");
    }

    fbx_log(FBX_LOG_DEBUG, "[fstore_retrieve] exit");
    return FILESTORE_OP_SUCCESS;
}

int fstore_exists(fstore_context *context, char *file_id, int *exists) {
    fbx_log(FBX_LOG_DEBUG, "[fstore_exists] enter");

    int texists;
    if(synclog_exists(context->synclog, file_id, &texists, NULL, NULL) == SYNCLOG_ERROR) {
        fbx_log(FBX_LOG_ERROR, "[fstore_exists] synclog error or not exist, exit");
        return FILESTORE_OP_FAILURE;
    }
    *exists = texists;

    fbx_log(FBX_LOG_DEBUG, "[fstore_exists] exit");
    return FILESTORE_OP_SUCCESS;
}

int fstore_available(fstore_context *context, char *file_id, int *available, char **destfile) {
    fbx_log(FBX_LOG_DEBUG, "[fstore_available] exit");

    int i;
    int nlocs = context->nlocs;
    char **loc_ids = context->loc_ids;
    char **locs = context->locs;

    int nlocs_avail = 0;
    char **locs_avail = 0;
    int texists;

    if(synclog_exists(context->synclog, file_id, &texists, &locs_avail, &nlocs_avail) == SYNCLOG_ERROR) {
        fbx_log(FBX_LOG_ERROR, "[fstore_available] synclog error or not exist, exit");
        return FILESTORE_OP_FAILURE;
    }
    
    *available = 0;
    if(!texists) {
        fbx_log(FBX_LOG_DEBUG, "[fstore_available] exit");
        return FILESTORE_OP_SUCCESS;
    }

    int max_try = 1 << ((nlocs < 10) ? nlocs : 10);
    for(i = 0; i < max_try; i++) {
        int randloc = rand() % nlocs;
        if(is_present(locs_avail, nlocs_avail, loc_ids[randloc])) {
            char source[strlen(locs[randloc]) + 1 + strlen(file_id) + 1 + 1];
            sprintf(source, "%s/%.*s/%s", locs[randloc], 2, file_id, file_id + 2);

            int exists = 0;
            if(file_exists(source, &exists) == FILEOP_FAILURE || !exists) {
                continue;
            }
            *available = 1;
            if(destfile) {
                *destfile = strdup(source);
            }
            break;
        }
    }

    if(nlocs_avail) {
        for(i = 0; i < nlocs_avail; i++) {
            free(locs_avail[i]);
        }
        free(locs_avail);
    } else {
        fbx_log(FBX_LOG_ERROR, "[fstore_available] file not available at any location");
    }

    fbx_log(FBX_LOG_DEBUG, "[fstore_available] exit");
    return FILESTORE_OP_SUCCESS;
}

int fstore_delete_request(fstore_context *context, char *file_id) {
    fbx_log(FBX_LOG_DEBUG, "[fstore_delete_request] enter");

    int exists;
    if(synclog_exists(context->synclog, file_id, &exists, NULL, NULL) == SYNCLOG_ERROR
        || !exists
        || synclog_delete(context->synclog, file_id, 0) == SYNCLOG_ERROR) {
        fbx_log(FBX_LOG_ERROR, "[fstore_delete_request] synclog error or not exist, exit");
        return FILESTORE_OP_FAILURE;
    }

    fbx_log(FBX_LOG_DEBUG, "[fstore_delete_request] exit");
    return FILESTORE_OP_SUCCESS;
}

int fstore_delete(fstore_context *context, char *file_id) {
    fbx_log(FBX_LOG_DEBUG, "[fstore_delete] enter");

    int i;
    int nlocs = context->nlocs;
    char **locs = context->locs;

    int failed_delete = 0;
    for(i = 0; i < nlocs; i++) {
        char *target = (char *)malloc(strlen(locs[i]) + 1 + strlen(file_id) + 1 + 1);
        sprintf(target, "%s/%.*s/%s", locs[i], 2, file_id, file_id + 2);
        
        //XXX [reco] take care of removing delete request
        //for the locations which are removed paremently
        int exists = 0;
        if(file_exists(target, &exists) == FILEOP_SUCCESS) {
            if(exists && file_delete(target) == FILEOP_FAILURE) {
                fbx_log(FBX_LOG_WARN, "[fstore_delete] delete failed at %s", target);
                failed_delete = 1;
            }
        } else {
            fbx_log(FBX_LOG_WARN, "[fstore_delete] delete failed at %s", target);
            failed_delete = 1;
        }

        free(target);
    }

    fbx_log(FBX_LOG_DEBUG, "[fstore_delete] exit");

    if(failed_delete)
        return FILESTORE_OP_FAILURE;

    return FILESTORE_OP_SUCCESS;
}

void fstore_cleanup(fstore_context *context) {
    fbx_log(FBX_LOG_DEBUG, "[fstore_cleanup] enter");
    if(context) {
        free(context);
    }
    fbx_log(FBX_LOG_DEBUG, "[fstore_cleanup] exit");
}


#if TEST_FILESTORE
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "testhelp.h"

int main(void) {
    int reply;
    struct stat f_stat;

    struct { char *host; int port; } rconf = {
       "127.0.0.1",
        6379
    };
    synclog_context *synclog = synclog_init(&rconf);

    if(!synclog) {
        fprintf(stderr, "synclog cannot be initialized");
        exit(1);
    }
    char *loc_ids[] = {"L1", "L2"};
    char *locs[] = {"/home/swapnil/Temp/filebox_test/L1", "/home/swapnil/Temp/filebox_test/L2"};

    fstore_context *fstore;
    fstore = fstore_init(synclog, 2, loc_ids, locs);
    if(!fstore) {
        synclog_cleanup(synclog);
        fprintf(stderr, "filestore cannot be initialized");
        exit(1);
    }

    reply = fstore_store_request(fstore, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "/home/swapnil/Temp/test.c");
    test_cond("fstore_store_request", reply == FILESTORE_OP_SUCCESS);

#if 1
    char *file_id = 0;
    char *somefile = 0;
    reply = synclog_next_add_request(synclog, &file_id, &somefile);
    test_cond("synclog_next_add_request", reply == SYNCLOG_SUCCESS
        && file_id && somefile);
    if(file_id) {
        fprintf(stderr, "file_id = %s\n", file_id);
        free(file_id); file_id = 0;
    }
    if(somefile) {
        fprintf(stderr, "somefile = %s\n", somefile);
        free(somefile); somefile = 0;
    }
#endif

    reply = fstore_store(fstore, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "/home/swapnil/Temp/test.c");
    test_cond("fstore_store", reply == FILESTORE_OP_SUCCESS
        && stat("/home/swapnil/Temp/filebox_test/L1/aa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", &f_stat) == 0 
        && stat("/home/swapnil/Temp/filebox_test/L2/aa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", &f_stat) == 0);

    int flag = 0;
    reply = fstore_exists(fstore, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", &flag);
    test_cond("fstore_exists", reply == FILESTORE_OP_SUCCESS && flag);

    flag = 0;
    reply = fstore_available(fstore, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", &flag, NULL);
    test_cond("fstore_available", reply == FILESTORE_OP_SUCCESS && flag);

    reply = fstore_delete_request(fstore, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    test_cond("fstore_delete_request", reply == FILESTORE_OP_SUCCESS);

#if 1
    file_id = 0;
    reply = synclog_next_delete_request(synclog, &file_id);
    test_cond("synclog_next_add_request", reply == SYNCLOG_SUCCESS
        && file_id);
    if(file_id) {
        fprintf(stderr, "file_id = %s\n", file_id);
        free(file_id); file_id = 0;
    }
#endif

    reply = fstore_delete(fstore, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    test_cond("fstore_delete", reply == FILESTORE_OP_SUCCESS
        && stat("/home/swapnil/Temp/filebox_test/L1/aa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", &f_stat) != 0 
        && stat("/home/swapnil/Temp/filebox_test/L2/aa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", &f_stat) != 0);

    flag = 0;
    reply = fstore_exists(fstore, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", &flag);
    test_cond("fstore_exists", reply == FILESTORE_OP_SUCCESS && !flag);

    flag = 0;
    reply = fstore_available(fstore, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", &flag, NULL);
    test_cond("fstore_available", reply == FILESTORE_OP_SUCCESS && !flag);


    fstore_cleanup(fstore);
    synclog_cleanup(synclog);

    test_report();

    return 0;
}

#endif
