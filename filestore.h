#ifndef __FBX_FILESTORE_H__
#define __FBX_FILESTORE_H__

#define FILESTORE_OP_SUCCESS  0
#define FILESTORE_OP_FAILURE -1

#include "synclog.h"

typedef struct {
    synclog_context *synclog;
    int nlocs;
    char **loc_ids;
    char **locs;
} fstore_context;

/* initialize filestore */
fstore_context *fstore_init(synclog_context *synclog, int nlocs, char *loc_ids[], char *locs[]);
/* stores given sourcefile in filestore */
int fstore_store_request(fstore_context *context, char *file_id, char *sourcefile); //creates request
int fstore_store(fstore_context *context, char *file_id, char *sourcefile); //actual deletion of the file
/* retrieve file with given in and content will be copied to destfile */
int fstore_retrieve(fstore_context *context, char *file_id, char *destfile); 
/* checks whehter given file_id exists in fielstore, exists will be set accordingly */
int fstore_exists(fstore_context *context, char *file_id, int *exists);
/* checks whether file exists and currently available to read. 
   also returns the location (destfile) from which it can be read */
int fstore_available(fstore_context *context, char *file_id, int *available, char **destfile);
/* deletes file from filestore */
int fstore_delete_request(fstore_context *context, char *file_id); //creates the request
int fstore_delete(fstore_context *context, char *file_id); //actual deletion of the file
/* filestore shutdown */
void fstore_cleanup(fstore_context *context);

#endif // __FBX_FILESTORE_H__
