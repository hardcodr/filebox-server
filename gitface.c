#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <git2.h>
#include <sds.h>

#include "common.h"
#include "gitface.h"

#define MAX_SHA1_SIZE 40
#define MIN_GIT_FILESIZE (MAX_SHA1_SIZE + 6 + 8 + 19)
#define MAX_ACC_COMMIT_THRESHOLD 100

const static char *YES_NO_INDICATOR[] = {"no","yes"};

static void handle_waiting_commits(git_context *context) {
    context->waiting_commits++;
    if(context->waiting_commits == MAX_ACC_COMMIT_THRESHOLD) {
        gitface_commit(context, "Autocommiting after crossing threshold");
        context->waiting_commits = 0;
    }
}

/**
 * create gitface context if repo is successfully opened or 
 * NULL.
 */
git_context *gitface_init(const char *repo_path) {
    fbx_log(FBX_LOG_DEBUG, "[gitface_init] enter");
    git_context *context = (git_context *)malloc(sizeof(git_context));
    if(context) {
        if(0 == git_repository_open(&context->repo, repo_path)) {
            context->repo_path = strdup(repo_path);
            context->waiting_commits = 0;
            fbx_log(FBX_LOG_DEBUG, "[gitface_init] exit");
            return context;
        }
        free(context);
    }
    fbx_log(FBX_LOG_DEBUG, "[gitface_init] exit");
    return NULL;
}

int gitface_add(git_context *context, const file_metadata *fm, const char *sha1, char **oldsha1) {
    fbx_log(FBX_LOG_DEBUG, "[gitface_add] enter");
    int retCode = GITFACE_OP_FAILURE;
    int exists = 0;
    char *gitpath = 0;
    char *gitfile = 0;
    char *gitrelpath = 0;
    char *tsha1= 0;
    sds s = 0;

    gitpath = path_join(context->repo_path, fm->relpath);
    if(FILEOP_SUCCESS == file_exists(gitpath, &exists)) {
       if(!exists && FILEOP_FAILURE == path_mkdirs(gitpath)) {
            fbx_log(FBX_LOG_ERROR, "[gitface_add] path creation failed");
            free(gitpath);
            fbx_log(FBX_LOG_DEBUG, "[gitface_add] exit");
            return FILEOP_FAILURE;
       }
    } else {
        fbx_log(FBX_LOG_ERROR, "[gitface_add] file exists operation failed");
        free(gitpath);
        fbx_log(FBX_LOG_DEBUG, "[gitface_add] exit");
        return FILEOP_FAILURE;
    }

    gitfile = path_join(gitpath, fm->filename);
    if(FILEOP_SUCCESS == file_exists(gitfile, &exists)) {
        if(exists) {
            char *file_content = 0;
            int filesize;
            if(FILEOP_FAILURE == file_read(gitfile, &file_content, &filesize)) {
                fbx_log(FBX_LOG_ERROR, "[gitface_add] read failed for file %s", gitfile);
                goto cleanup;
            }
            if(filesize >= MIN_GIT_FILESIZE) {
                tsha1 = (char *)malloc(MAX_SHA1_SIZE + 1);
                sscanf(file_content, "SHA-1:%s\n", tsha1);
            }
            free(file_content);
        }

        s = sdsempty();
        s = sdscatprintf(s, "SHA-1:%s\n", sha1);
        s = sdscatprintf(s, "Creator:%s\n", fm->creator);
        s = sdscatprintf(s, "Creation-Timestamp:%lld\n", fm->creation_ts);
        s = sdscatprintf(s, "Version:%d\n", fm->version);
        s = sdscatprintf(s, "Encrypted:%s\n", YES_NO_INDICATOR[fm->is_encrypted]);

        if(FILEOP_SUCCESS == file_write(gitfile, s, sdslen(s))) {
            git_index *index;
            if(0 == git_repository_index(&index, context->repo)) {
                gitrelpath = path_join(fm->relpath, fm->filename);
                if(0 == git_index_add_bypath(index, gitrelpath)
                        && 0 == git_index_write(index)) {
                    fbx_log(FBX_LOG_DEBUG, "[gitface_add] file %s successfully "
                        "added to gitface", gitfile);
                    handle_waiting_commits(context);
                    if(tsha1 && oldsha1)
                        *oldsha1 = strdup(tsha1);
                    retCode = GITFACE_OP_SUCCESS;
                }
                git_index_free(index);
            }
        }
    }

cleanup:
    if(gitpath)
        free(gitpath);
    if(gitfile)
        free(gitfile);
    if(gitrelpath)
        free(gitrelpath);
    if(tsha1)
        free(tsha1);
    if(s)
        sdsfree(s);

    fbx_log(FBX_LOG_DEBUG, "[gitface_add] exit");
    return retCode;
}

int gitface_complete(git_context *context, file_metadata *fm, char **sha1) {
    //XXX: ideally gitface should return the metadata and sha of the version
    //specified in the input, but currently it is ignored
    fbx_log(FBX_LOG_DEBUG, "[gitface_complete] enter");
    int retCode = GITFACE_OP_FAILURE;
    int exists = 0;
    char *gitpath = 0;
    char *gitfile = 0;

    gitpath = path_join(context->repo_path, fm->relpath);
    gitfile = path_join(gitpath, fm->filename);
    if(FILEOP_SUCCESS == file_exists(gitfile, &exists)) {
        if(exists) {
            int offset = 0;
            char tempsha1[MAX_SHA1_SIZE + 1];
            char tempcreator[1024];
            char tempts[21];
            char tempversion[11];
            char tempencrypted[5];

            char *file_content = 0;
            int filesize;
            if(FILEOP_FAILURE == file_read(gitfile, &file_content, &filesize)
                    || filesize < MIN_GIT_FILESIZE) {
                fbx_log(FBX_LOG_ERROR, "[gitface_complete] read failed for file %s", gitfile);
                goto cleanup;
            }

            sscanf(file_content, "SHA-1:%s\n", tempsha1);
            offset += 6 + strlen(tempsha1) + 1;
            sscanf(file_content + offset, "Creator:%s\n", tempcreator);
            offset += 8 + strlen(tempcreator) + 1;
            sscanf(file_content + offset, "Creation-Timestamp:%s\n", tempts);
            offset += 19 + strlen(tempts) + 1;
            sscanf(file_content + offset, "Version:%s\n", tempversion);
            offset += 8 + strlen(tempversion) + 1;
            sscanf(file_content + offset, "Encrypted:%s\n", tempencrypted);

            fm->version = atoi(tempversion);
            fm->is_encrypted = (0 == strcmp(tempencrypted, YES_NO_INDICATOR[0])) ? 0 : 1;
            fm->creator = strdup(tempcreator);
            fm->creation_ts = atoll(tempts);

            *sha1 = strdup(tempsha1);

            free(file_content);

            fbx_log(FBX_LOG_DEBUG, "[gitface_complete] metadata successfully "
                    "retrieved for file %s from gitface", gitfile);

            retCode = GITFACE_OP_SUCCESS;
        }
    }
cleanup:
    if(gitpath)
        free(gitpath);
    if(gitfile)
        free(gitfile);

    fbx_log(FBX_LOG_DEBUG, "[gitface_complete] exit");
    return retCode;
}

int gitface_reset(git_context *context, const file_metadata *fm) {
    fbx_log(FBX_LOG_DEBUG, "[gitface_reset] enter");

    int retCode = GITFACE_OP_FAILURE;

    char **paths = (char **)malloc(sizeof(char **));
    paths[0] = path_join(fm->relpath, fm->filename);
    git_checkout_opts opts = {
        .version = GIT_CHECKOUT_OPTS_VERSION,
        .checkout_strategy = GIT_CHECKOUT_FORCE,
        .disable_filters = 0,
        .dir_mode = 0,
        .file_mode = 0,
        .file_open_flags = 0,
        .notify_flags = GIT_CHECKOUT_NOTIFY_NONE,
        .notify_cb = 0,
        .notify_payload = 0,
        .progress_cb = 0,
        .progress_payload = 0,
        .paths = {
            .strings = paths,
            .count = 1
        }
    };

    //XXX: currently libgit2 does not obey .paths spec in opts 
    //and check's out entire depot 
    if(0 == git_checkout_head(context->repo, &opts)) {
        fbx_log(FBX_LOG_DEBUG, "[gitface_reset] reset successful");
        retCode = GITFACE_OP_SUCCESS;
    }

    free(paths[0]);
    free(paths);

    fbx_log(FBX_LOG_DEBUG, "[gitface_reset] exit");
    return retCode;
}

int gitface_remove(git_context *context, const file_metadata *fm, char **allsha1[], int *nallsha1, int *version) {
    fbx_log(FBX_LOG_DEBUG, "[gitface_remove] enter");

    int retCode = GITFACE_OP_FAILURE;
    int exists = 0;
    char *gitpath = 0;
    char *gitfile = 0;
    char *gitrelpath = 0;
    char *tsha1 = 0;

    gitpath = path_join(context->repo_path, fm->relpath);
    gitfile = path_join(gitpath, fm->filename);
    if(FILEOP_SUCCESS == file_exists(gitfile, &exists)) {
        if(exists) {
            char *file_content = 0;
            int filesize;
            if(FILEOP_FAILURE == file_read(gitfile, &file_content, &filesize)
                    || filesize < MIN_GIT_FILESIZE) {
                fbx_log(FBX_LOG_ERROR, "[gitface_remove] read failed for file %s", gitfile);
                goto cleanup;
            }

            tsha1 = (char *)malloc(MAX_SHA1_SIZE + 1);
            sscanf(file_content, "SHA-1:%s\n", tsha1);
            sscanf(strstr(file_content, "Version:"), "Version:%d\n", version);
            free(file_content);

            git_index *index;
            if(0 == git_repository_index(&index, context->repo)) {
                gitrelpath = path_join(fm->relpath, fm->filename);
                if(0 == git_index_remove(index, gitrelpath, 0)
                        && 0 == git_index_write(index)) {
                    handle_waiting_commits(context);
                    //XXX: support for returning multiple version
                    if(allsha1 && nallsha1) {
                        char ** sha1s = (char **)malloc(sizeof(char*));
                        sha1s[0] = strdup(tsha1);
                        *allsha1 = sha1s;
                        *nallsha1 = 1;
                        //remove file once deleted
                        unlink(gitfile);
                    }
                    fbx_log(FBX_LOG_DEBUG, "[gitface_remove] metadata successfully "
                            "removed for file %s from gitface", gitfile);
                    retCode = GITFACE_OP_SUCCESS;
                }
                git_index_free(index);
            }
        }
    }

cleanup:
    if(gitpath)
        free(gitpath);
    if(gitfile)
        free(gitfile);
    if(gitrelpath)
        free(gitrelpath);
    if(tsha1)
        free(tsha1);

    fbx_log(FBX_LOG_DEBUG, "[gitface_remove] exit");
    return retCode;
}

int gitface_commit(git_context *context, char *msg) {
    fbx_log(FBX_LOG_DEBUG, "[gitface_commit] enter");

    int retCode = GITFACE_OP_FAILURE;
    git_commit *p_commit = 0;
    git_signature *commiter = 0;
    git_index *index = 0;
    git_tree *tree = 0;

    if(!context->waiting_commits) {
        fbx_log(FBX_LOG_DEBUG, "[gitface_commit] nothing to commit, exit");
        return GITFACE_OP_SUCCESS;
    }

    git_oid oid;
    if(0 == git_reference_name_to_id(&oid, context->repo, "HEAD")) {
        git_commit_lookup(&p_commit, context->repo, &oid);
        git_signature_now(&commiter, "filebox", "filebox@hardcodr.com");
        git_repository_index(&index, context->repo);
    }

    if(p_commit && commiter && index) {
        git_oid tree_id;
        if(0 == git_index_write_tree(&tree_id, index)) {
            git_tree_lookup(&tree, context->repo, &tree_id);
            git_oid commit_id; //cache this to save git_reference_name_to_oid call above
            if(0 == git_commit_create_v(&commit_id, context->repo, "HEAD",
                        commiter, commiter, NULL, msg, tree, 1, p_commit)) {
                context->waiting_commits = 0;
                fbx_log(FBX_LOG_DEBUG, "[gitface_commit] %d changes commited "
                    "successfully", context->waiting_commits);
                retCode = GITFACE_OP_SUCCESS;
            } else {
                fbx_log(FBX_LOG_ERROR, "[gitface_commit] changes are not commited");
                //TODO: do something with given written index
            }
        }
    }

    if(p_commit)
        git_commit_free(p_commit);
    if(commiter)
        git_signature_free(commiter);
    if(index)
        git_index_free(index);
    if(tree)
        git_tree_free(tree);

    fbx_log(FBX_LOG_DEBUG, "[gitface_commit] exit");
    return retCode;
}

void gitface_cleanup(git_context *context) {
    fbx_log(FBX_LOG_DEBUG, "[gitface_cleanup] enter");
    if(context) {
        if(context->waiting_commits > 0) {
            gitface_commit(context, "Autocommit during cleanup");
        }
        free(context->repo_path);
        git_repository_free(context->repo);
        free(context);
    }
    fbx_log(FBX_LOG_DEBUG, "[gitface_cleanup] exit");
}

#ifdef TEST_GITFACE
#include <stdio.h>
#include <stdlib.h>
#include "testhelp.h"

typedef struct git_repository gitrepo;

int main(void) {
    int reply;
    git_context *context = gitface_init("/home/swapnil/Temp/git-test-repo");

    test_cond("git_init", context && context->repo);
    char *oldsha1;

    file_metadata fm1 = {
        .version= 5,
        .is_encrypted = 1,
        .relpath = "test.dir1",
        .filename = "test.file1",
        .creator = "swapnil",
        .creation_ts = 12345
    };
    
    char *sha1 = "11234aed09988998be2323bb8888999999999999";
    oldsha1 = 0;
    reply = gitface_add(context, &fm1, sha1, &oldsha1);
    test_cond("gitface_add", reply == GITFACE_OP_SUCCESS);
    if(oldsha1) {
        free(oldsha1);
        oldsha1 = 0;
    }

    reply = gitface_commit(context, "Test commit");
    test_cond("gitface_add", reply == GITFACE_OP_SUCCESS);


    file_metadata tempfm = {
        .relpath = "test.dir1",
        .filename = "test.file1",
    };
    char *outsha1 = 0;
    reply = gitface_complete(context, &tempfm, &outsha1);
    test_cond("gitface_complete", reply == GITFACE_OP_SUCCESS
        && tempfm.version == 5 && tempfm.is_encrypted == 1
        && 0 == strcmp(tempfm.creator, "swapnil")
        && tempfm.creation_ts == 12345
        && 0 == strcmp(sha1, outsha1));
    if(outsha1)
        free(outsha1);
    if(tempfm.creator)
        free(tempfm.creator);

    sha1 = "11234aed09988998be2323bb8888555555555555";
    oldsha1 = 0;
    reply = gitface_add(context, &fm1, sha1, &oldsha1);
    test_cond("gitface_add", reply == GITFACE_OP_SUCCESS);
    if(oldsha1) {
        free(oldsha1);
        oldsha1 = 0;
    }

    file_metadata fm2 = {
        .version= 0,
        .is_encrypted = 0,
        .relpath = "test.dir2",
        .filename = "test.file2",
        .creator = "swapnil",
        .creation_ts = 12345
    };
    sha1 = "11234aed09988998be2323bb8888111111111111";
    oldsha1 = 0;
    reply = gitface_add(context, &fm2, sha1, &oldsha1);
    test_cond("gitface_add", reply == GITFACE_OP_SUCCESS);
    if(oldsha1) {
        free(oldsha1);
    }

    reply = gitface_reset(context, &fm1);
    test_cond("gitface_reset", reply == GITFACE_OP_SUCCESS);

    char **allsha;
    int nallsha = 0;
    int version = 0;
    reply = gitface_remove(context, &fm1, &allsha, &nallsha, &version);
    test_cond("gitface_remove", reply == GITFACE_OP_SUCCESS && nallsha);
    if(allsha) {
        free(allsha[0]);
        free(allsha);
    }

    gitface_cleanup(context);
    test_report();

    return 0;
}

#endif
