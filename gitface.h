#ifndef __FBX_GITFACE_H__
#define __FBX_GITFACE_H__

#include <git2.h>

#define GITFACE_OP_SUCCESS  0
#define GITFACE_OP_FAILURE -1

typedef struct file_metadata {
    int version;
    int is_encrypted;
    char *relpath;
    char *filename;
    char *creator;
    long long creation_ts;
} file_metadata;

typedef struct git_context {
    char *repo_path;
    git_repository *repo;
    int waiting_commits;
} git_context;

git_context *gitface_init(const char *repo_path);
int gitface_add(git_context *context, const file_metadata *fm, const char *sha1, char **oldsha1);
int gitface_complete(git_context *context, file_metadata *fm, char **sha1);
int gitface_reset(git_context *context, const file_metadata *fm);
int gitface_remove(git_context *context, const file_metadata *fm, char **allsha1[], int *nallsha1, int *version);
int gitface_commit(git_context *context, char *msg);
void gitface_cleanup(git_context *context);

#endif // __FBX_GITFACE_H__
