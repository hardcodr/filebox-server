#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include "hiredis.h"

#include "common.h"
#include "synclog.h"

#define REDIS_KEY_FILE_PREFIX "F_"
#define REDIS_KEY_SYNCLOG_ADDLOG "SYNCLOG_ADDLOG"
#define REDIS_KEY_SYNCLOG_REMLOG "SYNCLOG_REMLOG"

typedef struct redis_config {
    char *host;
    int port;
} redis_config;

typedef struct redis_synclog_context {
    redisContext *r;
} redis_synclog_context;

/*
 * Returns NULL on error. Otherwise returns pointer to redis_synclog_context.
 */
synclog_context *synclog_init(synclog_config *config) {
    fbx_log(FBX_LOG_DEBUG, "[synclog_init] enter");

    redis_config *conf; 
    redis_synclog_context *rctx; 
    
    rctx = (redis_synclog_context *)malloc(sizeof(redis_synclog_context));
    conf = (redis_config *)config;

    rctx->r = redisConnect(conf->host, conf->port);
    if(rctx->r->err) {
        fbx_log(FBX_LOG_FATAL, "Cannot connect to redis instance at %s:%d, redis error = %s", 
            conf->host, conf->port, rctx->r->errstr);
    } else {
        return rctx;
    }

    redisFree(rctx->r);

    fbx_log(FBX_LOG_DEBUG, "[synclog_init] exit");
    return NULL;
}

int synclog_add(synclog_context *ctx, char *file_id, char *from) {
    fbx_log(FBX_LOG_DEBUG, "[synclog_add] enter");
    //assume that `from` is just simple filename satisfying regex [a-zA-Z/.-] 
    //and no other fancy characters, as they might mess up with internal logic.
    //specifically <SPACE> and <COMMA>.

    int retcode = SYNCLOG_SUCCESS;
    redis_synclog_context *rctx = (redis_synclog_context *)ctx;
    redisReply *reply;

    reply = redisCommand(rctx->r, "LPUSH %s %s,%s", REDIS_KEY_SYNCLOG_ADDLOG, file_id, from);
    if(reply->type == REDIS_REPLY_ERROR) {
        fbx_log(FBX_LOG_ERROR, "redis error = %s", reply->str);
        retcode = SYNCLOG_ERROR;
    }
    freeReplyObject(reply);

    fbx_log(FBX_LOG_DEBUG, "[synclog_add] exit");
    return retcode;
}

int synclog_next_add_request(synclog_context *ctx, char **file_id, char **from) {
    fbx_log(FBX_LOG_DEBUG, "[synclog_next_add_request] enter");

    int retcode = SYNCLOG_ERROR;
    redis_synclog_context *rctx = (redis_synclog_context *)ctx;
    redisReply *reply;

    reply = redisCommand(rctx->r, "LPOP %s", REDIS_KEY_SYNCLOG_ADDLOG);
    if(reply->type == REDIS_REPLY_STRING) {
        const char *loc = strchr(reply->str, ',');
        if(loc) {
            int file_id_len = loc - reply->str;
            int from_len = reply->len - file_id_len - 1;

            *file_id = (char *)malloc(sizeof(char)*(file_id_len + 1));
            sprintf(*file_id, "%.*s", file_id_len, reply->str);

            *from = (char *)malloc(sizeof(char)*(from_len + 1));
            sprintf(*from, "%.*s", from_len, loc + 1);

            retcode = SYNCLOG_SUCCESS;
        }
    }
    freeReplyObject(reply);

    fbx_log(FBX_LOG_DEBUG, "[synclog_next_add_request] exit");
    return retcode;
}

int synclog_sync_success(synclog_context *ctx, char *file_id, char *loc_id) {
    fbx_log(FBX_LOG_DEBUG, "[synclog_sync_success] enter");

    int retcode = SYNCLOG_SUCCESS;
    redis_synclog_context *rctx = (redis_synclog_context *)ctx;
    redisReply *reply;

    reply = redisCommand(rctx->r, "SADD %s%s %s", REDIS_KEY_FILE_PREFIX, file_id, loc_id);
    if(reply->type == REDIS_REPLY_ERROR) {
        fbx_log(FBX_LOG_ERROR, "redis error = %s", reply->str);
        retcode = SYNCLOG_ERROR;
    }
    freeReplyObject(reply);

    fbx_log(FBX_LOG_DEBUG, "[synclog_sync_success] exit");
    return retcode;
}

int synclog_exists(synclog_context *ctx, char *file_id, int *exists, char **loc_ids[], int *nlocs) {
    fbx_log(FBX_LOG_DEBUG, "[synclog_exists] enter");

    int retcode = SYNCLOG_SUCCESS;
    redis_synclog_context *rctx = (redis_synclog_context *)ctx;
    redisReply *reply;

    reply = redisCommand(rctx->r, "SMEMBERS %s%s", REDIS_KEY_FILE_PREFIX, file_id);
    if(reply->type == REDIS_REPLY_ARRAY && reply->elements > 0) {
        *exists = SYNCLOG_EXISTS;
        if(loc_ids && nlocs) {
            char **locs = (char **)malloc(sizeof(char *)*reply->elements);
            int i; for(i = 0; i < reply->elements; i++) {
                locs[i] = strdup(reply->element[i]->str);
            }
            *loc_ids = locs;
            *nlocs = reply->elements;
        }
    } else if(reply->type == REDIS_REPLY_ERROR) {
        fbx_log(FBX_LOG_ERROR, "redis error = %s", reply->str);
        retcode = SYNCLOG_ERROR;
    } else {
        *exists = SYNCLOG_NOT_EXISTS;
    }
    freeReplyObject(reply);

    fbx_log(FBX_LOG_DEBUG, "[synclog_exists] exit");
    return retcode;
}

int synclog_delete(synclog_context *ctx, char *file_id, int is_resubmit) {
    fbx_log(FBX_LOG_DEBUG, "[synclog_delete] enter");
    int retcode = SYNCLOG_SUCCESS;
    redis_synclog_context *rctx = (redis_synclog_context *)ctx;
    redisReply *reply;

    if(is_resubmit) {
        //for resubmit just add the delete request since the file record
        //is already removed
        reply = redisCommand(rctx->r, "LPUSH %s %s", REDIS_KEY_SYNCLOG_REMLOG, file_id);
        if(reply->type == REDIS_REPLY_ERROR) {
            fbx_log(FBX_LOG_ERROR, "redis error = %s", reply->str);
            retcode = SYNCLOG_ERROR;
        }
    } else {
        //XXX: do below two operation in single redis transaction
        reply = redisCommand(rctx->r, "DEL %s%s", REDIS_KEY_FILE_PREFIX, file_id);    
        if(reply->type == REDIS_REPLY_ERROR) {
            fbx_log(FBX_LOG_ERROR, "redis error = %s", reply->str);
            retcode = SYNCLOG_ERROR;
        } else {
            freeReplyObject(reply);

            reply = redisCommand(rctx->r, "LPUSH %s %s", REDIS_KEY_SYNCLOG_REMLOG, file_id);
            if(reply->type == REDIS_REPLY_ERROR) {
                fbx_log(FBX_LOG_ERROR, "redis error = %s", reply->str);
                retcode = SYNCLOG_ERROR;
            }
        }
    }
    freeReplyObject(reply);

    fbx_log(FBX_LOG_DEBUG, "[synclog_delete] exit");
    return retcode;
}

int synclog_next_delete_request(synclog_context *ctx, char **file_id) {
    fbx_log(FBX_LOG_DEBUG, "[synclog_next_delete_request] enter");

    int retcode = SYNCLOG_ERROR;
    redis_synclog_context *rctx = (redis_synclog_context *)ctx;
    redisReply *reply;

    reply = redisCommand(rctx->r, "LPOP %s", REDIS_KEY_SYNCLOG_REMLOG);
    if(reply->type == REDIS_REPLY_STRING) {
        *file_id = (char *)malloc(sizeof(char)*(reply->len + 1));
        sprintf(*file_id, "%.*s", reply->len, reply->str);

        retcode = SYNCLOG_SUCCESS;
    }
    freeReplyObject(reply);

    fbx_log(FBX_LOG_DEBUG, "[synclog_next_delete_request] exit");
    return retcode;
}


void synclog_cleanup(synclog_context *ctx) {
    fbx_log(FBX_LOG_DEBUG, "[synclog_cleanup] enter");
    redis_synclog_context *rctx = (redis_synclog_context *)ctx;

    if(rctx) {
        if(rctx->r)
            redisFree(rctx->r);

        free(rctx);
    }

    fbx_log(FBX_LOG_DEBUG, "[synclog_cleanup] exit");
}

#if 0
#ifdef TEST_REDIS_SYNCLOG
#include <stdio.h>
#include <stdlib.h>
#include "testhelp.h"

int main(void) {
    char *file_id;
    char *loc_id;
    char *loc_id_1;
    char *from;
    char *to;
    int reply;
    redis_config *rconf = (redis_config *)malloc(sizeof(redis_config));
    rconf->host = "127.0.0.1";
    rconf->port = 6379;

    synclog_context *ctx = synclog_init(rconf);
    if(ctx) {
        {
            int exists;

            file_id = "f1";

            reply = synclog_add(ctx, file_id);
            test_cond("add operation", reply == SYNCLOG_SUCCESS);

            reply = synclog_exists(ctx, file_id, &exists); 
            test_cond("exists operation", 
                reply ==  SYNCLOG_SUCCESS && exists == SYNCLOG_EXISTS);

            reply = synclog_delete(ctx, file_id);
            test_cond("delete operation", reply ==  SYNCLOG_SUCCESS);

            reply = synclog_exists(ctx, file_id, &exists); 
            test_cond("exists operation", 
                reply ==  SYNCLOG_SUCCESS && exists == SYNCLOG_NOT_EXISTS);
        }

        {
            char **loc_ids;
            int nlocs;

            file_id = "f1";
            loc_id = "l1";
            loc_id_1 = "l2";
            from = "/location/of/from/file";
            to = "/location/of/to/file";
           
            reply = synclog_log_failed_add(ctx, file_id, loc_id, from, to);
            test_cond("log-failed-add operation", reply == SYNCLOG_SUCCESS);

            reply = synclog_log_failed_add(ctx, file_id, loc_id_1, from, to);
            test_cond("log-failed-add operation", reply == SYNCLOG_SUCCESS);

            reply = synclog_get_failed_locs(ctx, file_id, &loc_ids, &nlocs);
            test_cond("get-failed-locs operation", 
                reply == SYNCLOG_SUCCESS && nlocs == 2);
            test_cond("get-failed-locs[0] operation", 
                strcmp(loc_ids[1], loc_id) == 0);
            free(loc_ids[1]);
            test_cond("get-failed-locs[1] operation", 
                strcmp(loc_ids[0], loc_id_1) == 0);
            free(loc_ids[0]);
            free(loc_ids);
        }

        synclog_cleanup(ctx);
    }
    free(rconf);
    test_report();
    return 0;
}

#endif
#endif
