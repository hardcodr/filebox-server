#include <fcgi_stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uriparser/Uri.h>

#include "common.h"
#include "gitface.h"
#include "synclog.h"
#include "filestore.h"
#include "request_handler.h"

git_context *gitface;
synclog_context *synclog; 
fstore_context *fstore;
fbx_request_handler_context* request_handler;

static const char *relativise(const char *rootpath, const char *fullpath) {
    int i, rootlen = strlen(rootpath);

    if(strlen(fullpath) < rootlen) {
        return NULL;
    }

    for(i = 0; i < rootlen; i++) {
        if(rootpath[i] != fullpath[i]) {
            return NULL;
        }
    }

    if(fullpath[i] == '/') {
        return fullpath + rootlen + 1;
    }
    return fullpath + rootlen;
}


void initialize() {
    char *conf_file = getenv("FBX_CONF_FILE");
    if(!conf_file) {
        conf_file = "filebox.conf";
    }

    char *git_repo = 0;

    char *synclog_host = 0;
    int synclog_port = 0;

    int location_count = 0;
    char **loc_ids;
    char **locations;

    int error_in_conf = read_conf(conf_file, &git_repo, &synclog_host, 
        &synclog_port, &location_count, &loc_ids, &locations, NULL, NULL);

    if(error_in_conf) {
        fprintf(stderr, "daemon terminating due to previous errors\n");
        exit(1);
    }

    git_context *gitface = gitface_init(git_repo);
    if(!gitface) {
        fprintf(stderr, "gitface cannot be initialized");
        exit(1);
    }

    struct { char *host; int port; } rconf = {
       synclog_host, synclog_port
    };

    synclog = synclog_init(&rconf);
    if(!synclog) {
        gitface_cleanup(gitface);
        fprintf(stderr, "synclog cannot be initialized\n");
        exit(1);
    }

    fstore = fstore_init(synclog, location_count, loc_ids, locations);
    if(!fstore) {
        gitface_cleanup(gitface);
        synclog_cleanup(synclog);
        fprintf(stderr, "filestore cannot be initialized\n");
        exit(1);
    }

    request_handler = handler_init(gitface, fstore);
    if(!request_handler) {
        fstore_cleanup(fstore);
        synclog_cleanup(synclog);
        gitface_cleanup(gitface);
        fprintf(stderr, "request handler cannot be initialized");
        exit(1);
    }
}

void terminate() {
    if(gitface) {
        gitface_cleanup(gitface);
    }

    if(synclog) {
        synclog_cleanup(synclog);
    }

    if(fstore) {
        fstore_cleanup(fstore);
    }

    if(request_handler) {
        handler_cleanup(request_handler);
    }
}

static void send_response(char *response, int is_error) {
    if(is_error) {
	printf ("Status: 500 Internal Server Error\r\n"
                "\r\n");
    }
    printf("Content-type: text/json\r\n"
           "\r\n");
    printf("%s\n", response);
}

static const unsigned long STDIN_MAX = 1000000;
static const char *POST_PARAMS_SEP = "Content-Disposition: form-data; name=\"";
static const int POST_PARAMS_SEP_LEN = 38;

int main(void) {
    int i;
    char *post_params;
    char *query_string;
    char *request_method;
    char *file_serve_path;
    char **request_param_names;
    char **request_param_values;

    fbx_log_verbosity = FBX_LOG_DEBUG;

    initialize();

    rstart: while(FCGI_Accept() >= 0) {
        post_params = 0;
        query_string = 0;
        request_method = 0;
        file_serve_path = 0;
        request_param_names = 0;
        request_param_values = 0;

        fbx_log(FBX_LOG_DEBUG, "started processing new request");

        request_method = getenv("REQUEST_METHOD");
        file_serve_path = getenv("SERVE_PATH");

        if(request_method) {
            fbx_log (FBX_LOG_DEBUG, "request_method = %s", request_method);
            if(0 == strcasecmp(request_method, "POST")) {
                int len = 0;
                char *clength = getenv("CONTENT_LENGTH");
                if (clength != NULL) {
                    len = strtol(clength, NULL, 10);
                }

                post_params = (char *)malloc(len + 1);
                query_string = (char *)malloc(len + 1);
                for(i = 0; i < len; i++) {
                    if((post_params[i] = getchar()) < 0) {
                        free(post_params);
                        free(query_string);
                        send_response("{ Return-Code:\"ERROR\", Error: \"Error while processing request header\"}", 1);
                        fbx_log(FBX_LOG_ERROR, "Invalid character read at %d",i);
                        goto rstart;
                    }
                }
                post_params[len] = 0;
                query_string[0] = '\0';
                
                char *tok = strstr(post_params, POST_PARAMS_SEP);
                int tlen = 0;
                while(tok) {
                    tok += POST_PARAMS_SEP_LEN;
                    tlen = strchr(tok, '\"') - tok;
                    strncat(query_string, tok, tlen);

                    strcat(query_string, "=");
                    
                    tok += tlen + 5;
                    tlen = strchr(tok, '\n') - tok;
                    strncat(query_string, tok, tlen-1);

                    strcat(query_string, "&");
                    
                    tok = strstr(tok, POST_PARAMS_SEP);
                }
            } else {
                query_string = strdup(getenv("QUERY_STRING"));
            }
            fbx_log(FBX_LOG_DEBUG, "Got %s request with QUERY_STRING = %s", request_method, query_string);

            UriQueryListA *queryList;
            int itemCount;
            if (uriDissectQueryMallocA(&queryList, &itemCount, query_string, 
                query_string + strlen(query_string)) != URI_SUCCESS) {
                send_response("{ Return-Code:\"ERROR\", Error: \"Incorrect query string parameter\"}", 1);
                if(post_params) {
                    free(post_params);
                }
                continue;
            }

            request_param_names = (char **)malloc(sizeof(char *)*itemCount);
            request_param_values = (char **)malloc(sizeof(char *)*itemCount);
            UriQueryListA *iterator = queryList;
            for(i = 0; i < itemCount; i++) {
                request_param_names[i] = strdup(iterator->key);
                request_param_values[i] = strdup(iterator->value);
                iterator = iterator->next;
            }

            fbx_params_t req_params = {
                itemCount, 
                request_param_names, 
                request_param_values
            };

            fbx_request_type_t req_type;
            if(0 == strcasecmp(request_method, "GET")) {
                req_type = FBX_REQUEST_GET;
            } else if(0 == strcasecmp(request_method, "POST")) {
                req_type = FBX_REQUEST_POST;
            } else if(0 == strcasecmp(request_method, "HEAD")) {
                req_type = FBX_REQUEST_HEAD;
            } else if(0 == strcasecmp(request_method, "DELETE")) {
                req_type = FBX_REQUEST_DEL;
            } else {
                send_response("{ Return-Code:\"ERROR\", Error:\"Incorrect request method\"}", 1);
                goto cleanup;
            }

            fbx_request_t request = {
                req_type, &req_params,
            };

            char *response = 0;
            if(handle_request(request_handler, &request, &response)== REQUEST_OK) {
                if(req_type == FBX_REQUEST_GET) { 
                    fbx_log(FBX_LOG_DEBUG, "Redirecting GET request to = %s", response);
                    if(file_serve_path) {
                        printf("Location: /filebox/serve/%s\n\n", relativise(file_serve_path, response));
                    } else {
                        printf("Location: /filebox/serve/%s\n\n", response);
                    }
                } else {
                    send_response(response, 0);
                }
            } else {
                send_response("{ Return-Code:\"ERROR\", Error: \"Error while processing request\"}", 1);
            }

            if(response) {
                free(response);
            }

cleanup:
            if(query_string) {
                free(query_string);
            }

            if(post_params) {
                free(post_params);
            }
            for(i = 0; i < itemCount; i++) {
                free(request_param_names[i]);
                free(request_param_values[i]);
            }
            free(request_param_names);
            free(request_param_values);
            uriFreeQueryListA(queryList);
        } else {
            send_response("{ Return-Code:\"ERROR\", Error: \"Missing request method\"}", 1);
        }
        fbx_log(FBX_LOG_DEBUG, "request processing ended");
    } /* while FCGI_Accept */

    terminate();

    return 0;
}
