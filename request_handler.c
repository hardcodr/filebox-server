#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#include <sds.h>

#include "common.h"
#include "gitface.h"
#include "filestore.h"
#include "request_handler.h"

const char *SEARCH_PARAMS[] = {
    "relpath",
    "filename",
    "version",
    "tempfile"
};

const char *DEL_PARAMS[] = {
    "relpath",
    "filename",
    "type"
};

const char *DEL_TYPES[] = {
    "soft",
    "hard"
};

const char *POST_PARAMS[] = {
    "relpath",
    "filename",
    "version",
    "is_encrypted",
    "creator",
    "creation_ts",
    "tempfile"
};

const char *DONE_PARAMS[] = {
    "done"
};

const static char *YES_NO_INDICATOR[] = {"no","yes"};

fbx_request_handler_context *handler_init(git_context *gitface, fstore_context *fstore) {
    fbx_request_handler_context *ctx = 
        (fbx_request_handler_context *)malloc(sizeof(fbx_request_handler_context));
    if(ctx) { 
        ctx->gitface = gitface;
        ctx->fstore = fstore;
    }
    return ctx;
}

static char *response_file_metadata(file_metadata *fm) {
    char *fmstr;
    sds s = sdsempty();

    s = sdscatprintf(s, "{");
    s = sdscatprintf(s, "Return-Code:\"OK\",");
    s = sdscatprintf(s, "File:\"%s/%s\",", fm->relpath, fm->filename);
    s = sdscatprintf(s, "Creator:\"%s\",", fm->creator);
    s = sdscatprintf(s, "Creation-Timestamp:%lld,", fm->creation_ts);
    s = sdscatprintf(s, "Version:%d,", fm->version);
    s = sdscatprintf(s, "Encrypted:\"%s\"", YES_NO_INDICATOR[fm->is_encrypted]);
    s = sdscatprintf(s, "}");

    fmstr = (char *)malloc(sdslen(s)+1);
    strcpy(fmstr, s);
    sdsfree(s);

    return fmstr;
}

int handle_post(git_context *gitface, fstore_context *fstore, fbx_request_t *request, char **out) {
    fbx_log(FBX_LOG_DEBUG, "enter handle_post method");

    int retcode = REQUEST_ERR;

    int i;
    char *tempfile = 0;
    char *sha1 = 0;
    char *oldsha1 = 0;
    
    char *msg = 0;

    fbx_params_t *params = request->request_params;
    file_metadata fm = {
        .relpath = 0,
        .filename = 0,
        .version = 0, //by default no versioning
        .is_encrypted = 0, //by default not encrypted
        .creator = 0,
        .creation_ts = time(NULL),
    };

    for(i = 0; i < params->nparams; i++) {
        if(0 == strcmp(params->params[i], POST_PARAMS[POST_PARAM_RELPATH])) {
            fm.relpath = strdup(params->values[i]);
        } else if(0 == strcmp(params->params[i], POST_PARAMS[POST_PARAM_FILENAME])) {
            fm.filename = strdup(params->values[i]);
        } else if(0 == strcmp(params->params[i], POST_PARAMS[POST_PARAM_VERSION])) {
            fm.version = atoi(params->values[i]);
        } else if(0 == strcmp(params->params[i], POST_PARAMS[POST_PARAM_ENC_IND])) {
            fm.is_encrypted = (0 == strcmp(params->values[i],YES_NO_INDICATOR[0])) ? 0 : 1;
        } else if(0 == strcmp(params->params[i], POST_PARAMS[POST_PARAM_CREATOR])) {
            fm.creator = strdup(params->values[i]);
        } else if(0 == strcmp(params->params[i], POST_PARAMS[POST_PARAM_CREAT_TS])) {
            fm.creation_ts = atoll(params->values[i]);
        } else if(0 == strcmp(params->params[i], POST_PARAMS[POST_PARAM_TEMPFILE])) {
            tempfile = strdup(params->values[i]);
        } else if(0 == strcmp(params->params[i], DONE_PARAMS[DONE_PARAM_MSG])) {
            msg = strdup(params->values[i]);
        } else {
            fbx_log(FBX_LOG_WARN, "Incorrect POST request param = %s", params->params[i]);
            goto cleanup;
        }
    }

    if(fm.relpath && fm.filename && fm.creator && tempfile) {
        fbx_log(FBX_LOG_DEBUG, "[handle_post] generating file sha1");
        sha1 = file_sha1(tempfile);

        if(!sha1) {
            fbx_log(FBX_LOG_ERROR, "Error while computing sha1 for tempfile = %s", tempfile);
            goto cleanup;
        }

        if(GITFACE_OP_FAILURE == gitface_add(gitface, &fm, sha1, &oldsha1)) {
            fbx_log(FBX_LOG_ERROR, "metadata add failed in git repo for file = %s/%s", fm.relpath, fm.filename);
            goto cleanup;
        }

        fbx_log(FBX_LOG_DEBUG, "[handle_post] storing file in filestore");
        if(FILESTORE_OP_SUCCESS == fstore_store_request(fstore, sha1, tempfile)) {
            if(oldsha1 && strcmp(sha1, oldsha1) && !fm.version) {
                fbx_log(FBX_LOG_DEBUG, "[handle_post] deleting old version for file with sha = %s", sha1);
                fstore_delete_request(fstore, oldsha1);
            }
            //TODO: offload commit activity to separate process
            if(msg && GITFACE_OP_FAILURE == gitface_commit(gitface, msg)) {
                fbx_log(FBX_LOG_ERROR, "git commit failed");
                goto cleanup;
            }
            *out = strdup("OK");
            retcode = REQUEST_OK;
        } else {
            fbx_log(FBX_LOG_ERROR, "filestore add failed for file = %s/%s", fm.relpath, fm.filename);
            //XXX:unless libgit resolves bug associated with
            //git-checkout, doing following operation will not 
            //produce intended result.
            //gitface_reset(gitface, &fm);
        }
    } else {
        if(msg && GITFACE_OP_FAILURE == gitface_commit(gitface, msg)) {
            fbx_log(FBX_LOG_ERROR, "git commit failed");
        } else {
            fbx_log(FBX_LOG_WARN, "One or more mandatory request parameters are not provided in POST request");
        }
    }

cleanup:
    if(fm.relpath)
        free(fm.relpath);
    if(fm.filename)
        free(fm.filename);
    if(fm.creator)
        free(fm.creator);
    if(tempfile)
        free(tempfile);
    if(sha1)
        free(sha1);
    if(oldsha1)
        free(oldsha1);
    if(msg)
        free(msg);

    fbx_log(FBX_LOG_DEBUG, "exit handle_post method");

    return retcode;
}


int handle_get(git_context *gitface, fstore_context *fstore, fbx_request_t *request, char **out) {
    fbx_log(FBX_LOG_DEBUG, "enter handle_get method");

    int retcode = REQUEST_ERR;

    int i;
    char *sha1 = 0;

    fbx_params_t *params = request->request_params;
    file_metadata fm = {
        .relpath = 0,
        .filename = 0,
        .version = 0
    };

    for(i = 0; i < params->nparams; i++) {
        assert(params->params[i] && params->values[i]);
        if(0 == strcmp(params->params[i], SEARCH_PARAMS[SEARCH_PARAM_RELPATH])) {
            fm.relpath = strdup(params->values[i]);
        } else if(0 == strcmp(params->params[i], SEARCH_PARAMS[SEARCH_PARAM_FILENAME])) {
            fm.filename = strdup(params->values[i]);
        } else if(0 == strcmp(params->params[i], SEARCH_PARAMS[SEARCH_PARAM_VERSION])) {
            fm.version = atoi(params->values[i]);
        } else {
            fbx_log(FBX_LOG_WARN, "Incorrect GET request param = %s", params->params[i]);
            goto cleanup;
        }
    }


    if(fm.relpath && fm.filename) {
        if(GITFACE_OP_FAILURE == gitface_complete(gitface, &fm, &sha1)) {
            fbx_log(FBX_LOG_ERROR, "git query failed for file = %s/%s", fm.relpath, fm.filename);
            goto cleanup;
        }

        int available = 0;
        char *destfile;
        if(FILESTORE_OP_FAILURE == fstore_available(fstore, sha1, &available, &destfile) || !available) {
            fbx_log(FBX_LOG_ERROR, "filestore get failed for file = %s/%s", fm.relpath, fm.filename);
            goto cleanup;
        }

        *out = destfile;
        retcode = REQUEST_OK;
    } else {
        fbx_log(FBX_LOG_WARN, "One or more mandatory request parameters are not provided in GET request");
    }

cleanup:
    if(fm.relpath)
        free(fm.relpath);
    if(fm.filename)
        free(fm.filename);
    if(fm.creator)
        free(fm.creator);
    if(sha1)
        free(sha1);

    fbx_log(FBX_LOG_DEBUG, "exit handle_get method");

    return retcode;
}

int handle_del(git_context *gitface, fstore_context *fstore, fbx_request_t *request, char **out ) {
    fbx_log(FBX_LOG_DEBUG, "enter handle_del method");

    int retcode = REQUEST_ERR;

    int i;
    char *sha1 = 0;

    fbx_params_t *params = request->request_params;
    file_metadata fm = {
        .relpath = 0,
        .filename = 0,
    };
    //For versioned files, if all the version needs to be removed then 
    //use the hard delete; otherwise it just removes the metadata and
    //keeps the file content
    //For non-versioned file any delete is always hard delete
    int is_hard = 0;

    for(i = 0; i < params->nparams; i++) {
        assert(params->params[i] && params->values[i]);
        if(0 == strcmp(params->params[i], DEL_PARAMS[DEL_PARAM_RELPATH])) {
            fm.relpath = strdup(params->values[i]);
        } else if(0 == strcmp(params->params[i], DEL_PARAMS[DEL_PARAM_FILENAME])) {
            fm.filename = strdup(params->values[i]);
        } else if(0 == strcmp(params->params[i], DEL_PARAMS[DEL_PARAM_TYPE])) {
            is_hard = (0 == strcmp(params->values[i],DEL_TYPES[0])) ? 0 : 1;
        } else {
            fbx_log(FBX_LOG_WARN, "Incorrect DEL request param = %s", params->params[i]);
            goto cleanup;
        }
    }


    if(fm.relpath && fm.filename) {
        char **allsha;
        int nallsha;
        int version = 0;
        if(GITFACE_OP_FAILURE == gitface_remove(gitface, &fm, &allsha, &nallsha, &version)) {
            fbx_log(FBX_LOG_ERROR, "git remove failed for file = %s/%s", fm.relpath, fm.filename);
            goto cleanup;
        }

        fbx_log(FBX_LOG_DEBUG, "Found version %d for file = %s/%s", version, fm.relpath, fm.filename);
        //if file is versioned (version > 0) then dont delete it physically,
        //otherwise delete it
        if(version) {
            if(is_hard) {
                for(i = 0; i < nallsha; i++) {
                    if(FILESTORE_OP_FAILURE == fstore_delete_request(fstore, allsha[i])) {
                        fbx_log(FBX_LOG_WARN, "filestore delete failed for file = %s/%s", fm.relpath, fm.filename);
                    }
                }
            } 
        } else {
            if(FILESTORE_OP_FAILURE == fstore_delete_request(fstore, allsha[0])) {
                fbx_log(FBX_LOG_WARN, "filestore delete failed for file = %s/%s", fm.relpath, fm.filename);
            }
        }
        *out = strdup("OK");
        retcode = REQUEST_OK;

        if(allsha) {
            free(allsha[0]);
            free(allsha);
        }
    } else {
        fbx_log(FBX_LOG_WARN, "One or more mandatory request parameters are not provided in DEL request");
    }

cleanup:
    if(fm.relpath)
        free(fm.relpath);
    if(fm.filename)
        free(fm.filename);
    if(fm.creator)
        free(fm.creator);
    if(sha1)
        free(sha1);

    fbx_log(FBX_LOG_DEBUG, "exit handle_del method");
    return retcode;

}

int handle_head(git_context *gitface, fbx_request_t *request, char **out) {
    int retcode = REQUEST_ERR;

    int i;
    char *sha1 = 0;

    fbx_params_t *params = request->request_params;
    file_metadata fm = {
        .relpath = 0,
        .filename = 0,
        .version = 0
    };

    for(i = 0; i < params->nparams; i++) {
        assert(params->params[i] && params->values[i]);
        if(0 == strcmp(params->params[i], SEARCH_PARAMS[SEARCH_PARAM_RELPATH])) {
            fm.relpath = strdup(params->values[i]);
        } else if(0 == strcmp(params->params[i], SEARCH_PARAMS[SEARCH_PARAM_FILENAME])) {
            fm.filename = strdup(params->values[i]);
        } else if(0 == strcmp(params->params[i], SEARCH_PARAMS[SEARCH_PARAM_VERSION])) {
            fm.version = atoi(params->values[i]);
        } else {
            fbx_log(FBX_LOG_WARN, "Incorrect HEAD request param = %s", params->params[i]);
            goto cleanup;
        }
    }


    if(fm.relpath && fm.filename) {
        if(GITFACE_OP_FAILURE == gitface_complete(gitface, &fm, &sha1)) {
            goto cleanup;
        }

        *out = response_file_metadata(&fm);
        retcode = REQUEST_OK;
    } else {
        fbx_log(FBX_LOG_WARN, "One or more mandatory request parameters are not provided in HEAD request");
    }

cleanup:
    if(fm.relpath)
        free(fm.relpath);
    if(fm.filename)
        free(fm.filename);
    if(fm.creator)
        free(fm.creator);
    if(sha1)
        free(sha1);

    return retcode;
}

/**
 * If successful returns the result in `out` variable and the sizeof(out) is returned as output.
 * On failure -1 is returned.
 */
int handle_request(fbx_request_handler_context *req_handler_ctx, fbx_request_t *request, char **out) {
    switch(request->type) {
        case FBX_REQUEST_POST:
            return handle_post(req_handler_ctx->gitface, req_handler_ctx->fstore, request, out);
        case FBX_REQUEST_GET:
            return handle_get(req_handler_ctx->gitface, req_handler_ctx->fstore, request, out);
        case FBX_REQUEST_DEL:
            return handle_del(req_handler_ctx->gitface, req_handler_ctx->fstore, request, out);
        case FBX_REQUEST_HEAD:
            return handle_head(req_handler_ctx->gitface, request, out);
    }
    return REQUEST_ERR;
}

void handler_cleanup(fbx_request_handler_context *req_handler_ctx) {
    if(req_handler_ctx) {
        free(req_handler_ctx);
    }
}

#ifdef TEST_REQUEST_HANDLER
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "testhelp.h"
#include "synclog.h"

int main(void) {
    int reply;
    git_context *gitface = gitface_init("/home/swapnil/Temp/filebox_test/gitrepo");
    if(!gitface) {
        fprintf(stderr, "gitface cannot be initialized");
        exit(1);
    }
    struct { char *host; int port; } rconf = {
       "127.0.0.1",
        6379
    };
    synclog_context *synclog = synclog_init(&rconf);

    if(!synclog) {
        gitface_cleanup(gitface);
        fprintf(stderr, "synclog cannot be initialized");
        exit(1);
    }
    char *loc_ids[] = {"L1", "L2"};
    char *locs[] = {"/home/swapnil/Temp/filebox_test/L1", "/home/swapnil/Temp/filebox_test/L2"};

    fstore_context *fstore;
    fstore = fstore_init(synclog, 2, loc_ids, locs);
    if(!fstore) {
        gitface_cleanup(gitface);
        synclog_cleanup(synclog);
        fprintf(stderr, "filestore cannot be initialized");
        exit(1);
    }

    fbx_request_handler_context* request_handler = handler_init(gitface, fstore);
    if(!request_handler) {
        fstore_cleanup(fstore);
        synclog_cleanup(synclog);
        gitface_cleanup(gitface);
        fprintf(stderr, "request handler cannot be initialized");
        exit(1);
    }
   
    char *put_param_names[] = {"relpath","filename","version","is_encrypted","creator","creation_ts","tempfile"};
    char *put_param_values[] = {
        "somedir1",
        "somefile1",
        "0",
        "no",
        "swapnil",
        "1234567890",
        "/home/swapnil/Temp/filebox_test/test.c"
    };
    fbx_params_t put_params = {
        7, put_param_names, put_param_values
    };
    fbx_request_t put_request = {
       FBX_REQUEST_POST, &put_params,
    };
    char *out = 0;

    reply = handle_request(request_handler, &put_request, &out);
    test_cond("handle_request(PUT)", reply == REQUEST_OK && out);
    if(out) {
        printf("Response = %s\n", out);
        free(out);
    }

    struct stat f_stat;

    char *get_param_names[] = {"relpath","filename","version","tempfile"};
    char *get_param_values[] = {
        "somedir1",
        "somefile1",
        "0",
        "/home/swapnil/Temp/filebox_test/test.c"
    };
    fbx_params_t t_params = {
        4, get_param_names, get_param_values
    };

    fbx_request_t t_request = {
       FBX_REQUEST_GET, &t_params,
    };
    out = 0;
    reply = handle_request(request_handler, &t_request, &out);
    test_cond("handle_request(GET)", reply == REQUEST_OK && out
            && stat(out, &f_stat) == 0);
    if(out) {
        printf("Response = %s\n", out);
        free(out);
    }

    t_params.nparams = 3;
    t_request.type = FBX_REQUEST_HEAD;
    out = 0;
    reply = handle_request(request_handler, &t_request, &out);
    test_cond("handle_request(HEAD)", reply == REQUEST_OK && out);
    if(out) {
        printf("Response = %s\n", out);
        free(out);
    }

    char *del_param_names[] = {"relpath","filename","type"};
    char *del_param_values[] = {
        "somedir1",
        "somefile1",
        "hard",
    };

    t_params.nparams = 3;
    t_params.params = del_param_names;
    t_params.values = del_param_values;
    t_request.type = FBX_REQUEST_DEL;
    out = 0;
    reply = handle_request(request_handler, &t_request, &out);
    test_cond("handle_request(DEL)", reply == REQUEST_OK && out);
    if(out) {
        printf("Response = %s\n", out);
        free(out);
    }

    fstore_cleanup(fstore);
    synclog_cleanup(synclog);
    gitface_cleanup(gitface);
    handler_cleanup(request_handler);

    test_report();
    
    return 0;
}

#endif
