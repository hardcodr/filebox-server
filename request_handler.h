#ifndef __FBX_REQUEST_HANDLER__
#define __FBX_REQUEST_HANDLER__

#include "gitface.h"
#include "filestore.h"

#define REQUEST_OK   0
#define REQUEST_ERR -1

typedef enum {
    FBX_REQUEST_POST = 0,
    FBX_REQUEST_GET  = 1,
    FBX_REQUEST_DEL  = 2,
    FBX_REQUEST_HEAD = 3,
} fbx_request_type_t;

//used in GET and HEAD operation
extern const char *SEARCH_PARAMS[];
enum {
    SEARCH_PARAM_RELPATH  = 0,
    SEARCH_PARAM_FILENAME = 1,
    SEARCH_PARAM_VERSION = 2,
    SEARCH_PARAM_TEMPFILE = 3,
    SEARCH_PARAM_COUNT
};

//use in DEL opreation
extern const char *DEL_PARAMS[];
enum {
    DEL_PARAM_RELPATH  = 0,
    DEL_PARAM_FILENAME = 1,
    DEL_PARAM_TYPE = 2,
    DEL_PARAM_COUNT
};

extern const char *DEL_TYPES[];
enum {
    DEL_TYPE_SOFT,
    DEL_TYPE_HARD
};

//used in POST operation
extern const char *POST_PARAMS[];
enum {
    POST_PARAM_RELPATH  = 0,
    POST_PARAM_FILENAME = 1,
    POST_PARAM_VERSION = 2,
    POST_PARAM_ENC_IND = 3,
    POST_PARAM_CREATOR = 4,
    POST_PARAM_CREAT_TS = 5,
    POST_PARAM_TEMPFILE = 6,
    POST_PARAM_COUNT
};

//used in DONE operation
extern const char *DONE_PARAMS[];
enum {
    DONE_PARAM_MSG = 0,
};


typedef struct {
    int nparams;
    char **params;
    char **values;
} fbx_params_t;

typedef struct {
    fbx_request_type_t type;
    fbx_params_t *request_params;
} fbx_request_t;

typedef struct {
    git_context *gitface;
    fstore_context *fstore;
} fbx_request_handler_context;

//functions
fbx_request_handler_context *handler_init(git_context *gitface, fstore_context *fstore);
int handle_request(fbx_request_handler_context *req_handler_ctx, fbx_request_t *request, char **out);
void handler_cleanup(fbx_request_handler_context *req_handler_ctx);

#endif //__FBX_REQUEST_HANDLER__
