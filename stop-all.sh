#!/bin/sh

echo "stopping cgi agent"
pid=`ps -u $USER | grep fcgiwrap | cut -b1-5`
kill -9 $pid

echo "stopping filebox sync damoen"
pid=`ps -u $USER | grep sync_daemon | cut -b1-5`
kill -9 $pid

echo "stopping filebox cgi server"
pid=`ps -u $USER | grep request_acc | cut -b1-5`
kill -9 $pid

cd ~/Tools/nginx-1.2.7/

echo "stopping nginx"
installed/sbin/nginx -s stop

cd -
