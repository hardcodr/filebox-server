#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "common.h"
#include "synclog.h"
#include "filestore.h"


static int max_sleeptime = 5;
static int init_sleeptime = 100000000;

static struct timespec remaining;
static struct timespec sleeptime;

static void daemon_sleep(int idle_cycle) {
    //TODO: improve this to accept hints from the 
    // the CGI proc for put request
    if(idle_cycle) {
        if(sleeptime.tv_sec < max_sleeptime) {
            sleeptime.tv_nsec *= 2;

            if(sleeptime.tv_nsec > 999999999L) {
                sleeptime.tv_nsec = 0;
                sleeptime.tv_sec = 1;
            } else {
                sleeptime.tv_sec *= 2;
            }

            if(sleeptime.tv_sec > max_sleeptime) {
                sleeptime.tv_sec = max_sleeptime;
            }
        }
    } else {
        //reset the sleeptime
        sleeptime.tv_sec = 0;
        sleeptime.tv_nsec = init_sleeptime;
    }

    printf("sleeping for %ld sec %ld nsec\n", sleeptime.tv_sec, sleeptime.tv_nsec);

    //ignore interrupts and etc for now.
    nanosleep(&sleeptime, &remaining);
}

int main(int argc, char *argv[]) {
    char *conf_file;
    if(argc > 1) {
        conf_file = argv[1]; 
    } else {
        conf_file = "filebox.conf";
    }

    char *synclog_host = 0;
    int synclog_port = 0;

    int location_count = 0;
    char **loc_ids;
    char **locations;

    int error_in_conf = read_conf(conf_file, NULL, &synclog_host, 
        &synclog_port, &location_count, &loc_ids, &locations,
        &init_sleeptime, &max_sleeptime);

    fbx_log_verbosity = FBX_LOG_ERROR;

    if(error_in_conf) {
        fprintf(stderr, "daemon terminating due to previous errors\n");
        return 1;
    }

    struct { char *host; int port; } rconf = {
       synclog_host, synclog_port
    };

    synclog_context *synclog = synclog_init(&rconf);
    if(!synclog) {
        fprintf(stderr, "synclog cannot be initialized\n");
        return 1;
    }

    fstore_context *fstore = fstore_init(synclog, location_count, loc_ids, locations);
    if(!fstore) {
        synclog_cleanup(synclog);
        fprintf(stderr, "filestore cannot be initialized\n");
        return 1;
    }

    sleeptime.tv_sec = 0;
    sleeptime.tv_nsec = init_sleeptime;

    for(;;) {
        int idle_cycle = 1;

        char *file_id = 0;
        char *sourcefile = 0;

        if(synclog_next_add_request(synclog, &file_id, &sourcefile) 
            == SYNCLOG_SUCCESS) {

            int exists = 0;
            if(fstore_exists(fstore, file_id, &exists) == FILESTORE_OP_SUCCESS  && exists) {
                //file already at the newest version; ignore the request
                unlink(sourcefile);
            } else {
                if(fstore_store(fstore, file_id, sourcefile) == FILESTORE_OP_FAILURE) {
                    //XXX: [reco] handling location which is permently removed
                    synclog_add(synclog, file_id, sourcefile);
                } else {
                    unlink(sourcefile);
                }
            }
            free(file_id); 
            free(sourcefile);

            idle_cycle = 0;
        }

        if(synclog_next_delete_request(synclog, &file_id) 
            == SYNCLOG_SUCCESS) {
            if(fstore_delete(fstore, file_id) == FILESTORE_OP_FAILURE) {
                //XXX: [reco] handling location which is permently removed
                synclog_delete(synclog, file_id, 1);
            }
            free(file_id); 

            idle_cycle = 0;
        }
        
        daemon_sleep(idle_cycle);
    }

    fstore_cleanup(fstore);
    synclog_cleanup(synclog);

    return 0;
}
