#ifndef __FBX_SYNCLOG_H__
#define __FBX_SYNCLOG_H__

#define SYNCLOG_SUCCESS  0
#define SYNCLOG_ERROR   -1

#define SYNCLOG_NOT_EXISTS  0
#define SYNCLOG_EXISTS      1

typedef void synclog_config;
typedef void synclog_context;

//== functions ==
//initializes synclog
synclog_context *synclog_init(synclog_config *config);
//adds ADD sync record for file-id, `from` is the source file name
int synclog_add(synclog_context *ctx, char *file_id, char *from);
int synclog_next_add_request(synclog_context *ctx, char **file_id, char **from);
//marks the given location's sync to be complete
int synclog_sync_success(synclog_context *ctx, char *file_id, char *loc_id);
//checks if the file exists, if exists then returns the location where it is available
int synclog_exists(synclog_context *ctx, char *file_id, int *exists, char **loc_ids[], int *nlocs);
//adds DEL sync record for given file-id
int synclog_delete(synclog_context *ctx, char *file_id, int is_resubmit);
int synclog_next_delete_request(synclog_context *ctx, char **file_id);
//cleans up synclog
void synclog_cleanup(synclog_context *ctx);

#endif //__FBX_SYNCLOG_H__
